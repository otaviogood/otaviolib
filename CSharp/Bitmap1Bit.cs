﻿#region ApacheLicense2.0
// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion
#define BITMAP1BIT

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Otavio.Math;

namespace Otavio.Image
{
	public sealed class Bitmap1Bit
	{
		private int width, height;
		public int Width
		{
			get { return width; }
			set { width = value; }
		}
		public int Height
		{
			get { return height; }
			set { height = value; }
		}

		public uint[] colorBuffer;

		public Bitmap1Bit(int w, int h)
		{
			ResetSize(w, h);
		}

		public Bitmap1Bit(Bitmap1Bit orig)
		{
			ResetSize(orig.Width, orig.Height);
			Array.Copy(orig.colorBuffer, colorBuffer, colorBuffer.Length);
		}

		//#if BITMAPGRAY
		public Bitmap1Bit(BitmapGray source)
		{
			ResetSize(source.Width, source.Height);
			int sourceIndex = 0;
			int destIndex = 0;
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < (width >> 5); x++)
				{
					uint totalBits = 0;
					byte[] sourceData = source.m_color;
					for (int bitCount = 0; bitCount < 32; bitCount++)
					{
						uint pix = sourceData[sourceIndex];
						totalBits >>= 1;
						totalBits |= ((pix & 0x80) << 24);	// threshold at < 128. (0x80)
						++sourceIndex;
					}
					// write out 32 1 bit pixels
					colorBuffer[destIndex] = totalBits;
					++destIndex;
				}
			}
		}
		//#endif

		public void ResetSize(int w, int h)
		{
			Debug.Assert((w / 32) * 32 == w);
			Height = h;
			Width = w;
			colorBuffer = new uint[(Width * Height) >> 5];
		}

		bool IsInBounds(int x, int y)
		{
			if (x < 0) return false;
			if (y < 0) return false;
			if (x >= Width) return false;
			if (y >= Height) return false;
			return true;
		}

		public uint GetPixel(int x, int y)
		{
			int index = (x + y * width);
			return (colorBuffer[index >> 5] >> (index & 0x1f)) & 1;
		}

		uint GetPixel(int index)
		{
			return (colorBuffer[index >> 5] >> (index & 0x1f)) & 1;
		}

		public void Invert()
		{
			for (int i = 0; i < colorBuffer.Length; i++)
			{
				colorBuffer[i] = ~colorBuffer[i];
			}
		}

		public void SetPixel(int x, int y)
		{
			int index = (x + y * width);
			colorBuffer[index >> 5] |= ((uint)1) << (index & 0x1f);
		}

		void ClearPixel(int x, int y)
		{
			int index = (x + y * width);
			colorBuffer[index >> 5] &= ~(((uint)1) << (index & 0x1f));
		}

		public void MakeBorder()
		{
			for (int i = 0; i < width; i++)
			{
				SetPixel(i, 0);
				SetPixel(i, height - 1);
			}
			for (int i = 0; i < height; i++)
			{
				SetPixel(0, i);
				SetPixel(width - 1, i);
			}
		}

		private void FindSpan(ref int current)
		{
			uint temp = colorBuffer[current >> 5];
			temp >>= current & 0x1f;
			while (((current & 0x1f) != 0))
			{
				if ((temp & 0x00000001) != 0) return;
				temp >>= 1;
				++current;
			}
			while (colorBuffer[current >> 5] == 0)
			{
				current += 32;
			}
			temp = colorBuffer[current >> 5];
			while ((temp & 0x00000001) == 0)
			{
				temp >>= 1;
				++current;
			}
		}

		// fills in a 1 bit line from start to end, inclusively.
		private void FillSpan(int start, int end)
		{
			uint current = (uint)start;
			uint temp = colorBuffer[start >> 5];
			// this loop can be done with 1 operation.? maybe.
			while (((current & 0x1f) != 0) && (current <= end))
			{
				temp |= ((uint)1) << ((int)current & 0x1f);
				++current;
			}
			colorBuffer[start >> 5] = temp;

			// rage out - fill 32 bits at a time once the fill is aligned.
			while (current + 31 <= end)
			{
				colorBuffer[current >> 5] = 0xffffffff;
				current += 32;
			}

			// Fill the last few bits.
			// this loop can be done with 1 operation.
			if ((current <= end))
			{
				temp = colorBuffer[end >> 5];
				temp |= ((uint)0xffffffff) >> (31 - (end - (int)current));
				colorBuffer[end >> 5] = temp;
			}
		}

		// inclusive start and end
		private bool Find0(ref int current, int end)
		{
			while (current <= end)
			{
				if (GetPixel(current) == 0) return true;
				current++;
			}
			return false;
		}

		private bool Find1(ref int current, int end)
		{
			while (current <= end)
			{
				if (GetPixel(current) == 1) return true;
				current++;
			}
			return false;
		}

		private bool FindSpanBounded0(ref int current, int end)
		{
			uint temp = colorBuffer[current >> 5];
			// this loop can be done with 1 operation.? maybe.
			while (((current & 0x1f) != 0) && (current <= end))
			{
				if ((temp & (1 << (current & 0x1f))) == 0) return true;
				++current;
			}

			while (current + 31 <= end)
			{
				if (colorBuffer[current >> 5] != 0xffffffff) break;
				current += 32;
			}

			temp = colorBuffer[current >> 5];
			while ((current <= end))
			{
				if ((temp & (1 << (current & 0x1f))) == 0) return true;
				++current;
			}
			return false;
		}

		private bool FindSpanBounded1(ref int current, int end)
		{
			uint temp = colorBuffer[current >> 5];
			// this loop can be done with 1 operation.? maybe.
			while (((current & 0x1f) != 0) && (current <= end))
			{
				if ((temp & (1 << (current & 0x1f))) != 0) return true;
				++current;
			}

			while (current + 31 <= end)
			{
				if (colorBuffer[current >> 5] != 0) break;
				current += 32;
			}

			temp = colorBuffer[current >> 5];
			while ((current <= end))
			{
				if ((temp & (1 << (current & 0x1f))) != 0) return true;
				++current;
			}
			return false;
		}

		public bool FloodFill(out BoxI2 bounds, int2 pixPos, out List<int2> pixelList)
		{
			bounds = new BoxI2();
			pixelList = new List<int2>();
			Stack<int2> stack = new Stack<int2>();

			int minx = 32000;
			int maxx = 0;
			int miny = 32000;
			int maxy = 0;
			int pixelCount = 0;
			do
			{
				int startX = pixPos.y * width + pixPos.x;
				int endX;
				int pixIndex = startX;
				FindSpan(ref pixIndex);
				pixPos.x += pixIndex - startX;
				// if we successfully filled at least one pixel...
				if (pixIndex != startX)
				{
					pixelCount = pixIndex - startX;
					int2 currentSpot = new int2(pixPos.x - 1, pixPos.y);
					pixelList.Add(currentSpot);
					minx = System.Math.Min(currentSpot.x, minx);
					maxx = System.Math.Max(currentSpot.x, maxx);
					miny = System.Math.Min(currentSpot.y, miny);
					maxy = System.Math.Max(currentSpot.y, maxy);
					bounds.MergePoint(currentSpot);

					endX = pixIndex;
					pixIndex = startX - 1;
					pixPos.x += pixIndex - endX;
					while (GetPixel(pixIndex) == 0)
					{
						pixelCount++;
						pixIndex -= 1;
						pixPos.x -= 1;
					}
					//			if (pixIndex != startX - 1)	// oops. this wasn't filling the left-hand edge. :(
					{
						int2 currentSpot2 = new int2(pixPos.x + 1, pixPos.y);
						pixelList.Add(currentSpot2);
						minx = System.Math.Min(currentSpot2.x, minx);
						maxx = System.Math.Max(currentSpot2.x, maxx);
						bounds.MergePoint(currentSpot2);
					}

					FillSpan(pixIndex + 1, endX - 1);

					int current = pixIndex + 1 + width;
					int2 currentPixPos = pixPos + new int2(1, 1);
					while (true)
					{
						int orig = current;
						bool found = Find0(ref current, (endX - 1) + width);
						currentPixPos.x += current - orig;
						if (!found) break;
						stack.Push(currentPixPos);
						orig = current;
						found = FindSpanBounded1(ref current, (endX - 1) + width);
						currentPixPos.x += current - orig;
						if (!found) break;
					}

					current = pixIndex + 1 - width;
					currentPixPos = pixPos + new int2(1, -1);
					while (true)
					{
						int orig = current;
						bool found = FindSpanBounded0(ref current, (endX - 1) - width);
						currentPixPos.x += current - orig;
						if (!found) break;
						stack.Push(currentPixPos);
						orig = current;
						found = Find1(ref current, (endX - 1) - width);
						currentPixPos.x += current - orig;
						if (!found) break;
					}

				}

				if (stack.Count == 0) break;
				pixPos = stack.Pop();
				pixIndex = pixPos.y * width + pixPos.x;

			} while (true);

			if (pixelCount == 0) return false;
			bounds.m_min.x = minx;
			bounds.m_min.y = miny;
			bounds.m_max.x = maxx;
			bounds.m_max.y = maxy;
			return true;
		}

	}
}

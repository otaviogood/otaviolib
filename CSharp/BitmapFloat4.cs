#region ApacheLicense2.0
// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion
#define BITMAPFLOAT4

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Otavio.Math;

namespace Otavio.Image
{
	public class BitmapFloat4
	{
		private int m_width, m_height;
		public int Width
		{
			get { return m_width; }
			set { m_width = value; }
		}
		public int Height
		{
			get { return m_height; }
			set { m_height = value; }
		}

		public float[, ,] m_color;

		public BitmapFloat4()
		{
			ResetSize(8, 8);
		}

		public BitmapFloat4(int width, int height)
		{
			ResetSize(width, height);
		}

		public BitmapFloat4(string fileName)
		{
			FileLoad(fileName);
		}

		public BitmapFloat4(BitmapFloat4 orig)
		{
			m_width = orig.m_width;
			m_height = orig.m_height;
			m_color = new float[orig.Width, orig.Height, 4];
			m_color = (float[, ,])orig.m_color.Clone();
		}
#if BITMAPARGB
		public BitmapFloat4(BitmapARGB orig)
		{
			m_width = orig.Width;
			m_height = orig.Height;
			m_color = new float[orig.Width, orig.Height, 4];
			for (int y = 0; y < orig.Height; y++)
			{
				for (int x = 0; x < orig.Width; x++)
				{
					SetPixel4(x, y, orig.GetPixel4(x, y));	// slow. sorry.
				}
			}
		}
#endif

		public void ResetSize(int w, int h)
		{
			Height = h;
			Width = w;
			m_color = new float[Width, Height, 4];
		}

		public void Clear(float4 col)
		{
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					m_color[x, y, 0] = col.x;
					m_color[x, y, 1] = col.y;
					m_color[x, y, 2] = col.z;
					m_color[x, y, 3] = col.w;
				}
			}
		}

		bool IsInBounds(int x, int y)
		{
			if (x < 0) return false;
			if (y < 0) return false;
			if (x >= Width) return false;
			if (y >= Height) return false;
			return true;
		}

		public float3 GetPixel3(int x, int y)
		{
			float4 p4 = GetPixel4(x, y);
			return new float3(p4.x, p4.y, p4.z);
		}

		public float4 GetPixel4(int x, int y)
		{
			if (x >= Width) x = Width - 1;
			if (x < 0) x = 0;
			if (y >= Height) y = Height - 1;
			if (y < 0) y = 0;
			return new float4(m_color[x, y, 0], m_color[x, y, 1], m_color[x, y, 2], m_color[x, y, 3]);
		}

		public float4 GetPixelBilinear4(float2 pos)
		{
			if (pos.x >= Width) pos.x = Width - 1;
			if (pos.x < 0) pos.x = 0;
			if (pos.y >= Height) pos.y = Height - 1;
			if (pos.y < 0) pos.y = 0;
			float2 floor = new float2((float)System.Math.Floor(pos.x), (float)System.Math.Floor(pos.y));
			float2 remainder = pos - floor;
			float4 pix00 = GetPixel4((int)floor.x, (int)floor.y);
			float4 pix10 = GetPixel4((int)floor.x + 1, (int)floor.y);
			float4 pix01 = GetPixel4((int)floor.x, (int)floor.y + 1);
			float4 pix11 = GetPixel4((int)floor.x + 1, (int)floor.y + 1);

			float4 pixX0 = pix10 * remainder.x + pix00 * (1.0f - remainder.x);
			float4 pixX1 = pix11 * remainder.x + pix01 * (1.0f - remainder.x);
			float4 pixY = pixX1 * remainder.y + pixX0 * (1.0f - remainder.y);
			return pixY;
		}

		public void SetPixel4(int x, int y, float4 val)
		{
			if (x >= Width) return;
			if (x < 0) return;
			if (y >= Height) return;
			if (y < 0) return;
			m_color[x, y, 0] = val.x;
			m_color[x, y, 1] = val.y;
			m_color[x, y, 2] = val.z;
			m_color[x, y, 3] = val.w;
		}

		public void SetPixel3(int x, int y, float3 val)
		{
			if (x >= Width) return;
			if (x < 0) return;
			if (y >= Height) return;
			if (y < 0) return;
			m_color[x, y, 0] = val.x;
			m_color[x, y, 1] = val.y;
			m_color[x, y, 2] = val.z;
		}

		public void AddPixel4(int x, int y, float4 val)
		{
			if (x >= Width) return;
			if (x < 0) return;
			if (y >= Height) return;
			if (y < 0) return;
			m_color[x, y, 0] += val.x;
			m_color[x, y, 1] += val.y;
			m_color[x, y, 2] += val.z;
			m_color[x, y, 3] += val.w;
		}

		public System.Drawing.Bitmap Draw(bool UseAlpha)
		{
			if (m_color == null) return null;

			System.Drawing.Bitmap finalColorBitmap = null;
			if (finalColorBitmap == null) finalColorBitmap = new System.Drawing.Bitmap(Width, Height);
			System.Drawing.Imaging.PixelFormat pf = System.Drawing.Imaging.PixelFormat.Format24bppRgb;
			if (UseAlpha) pf = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
			// GDI+ still lies to us - the return format is BGR, NOT RGB.
			System.Drawing.Imaging.BitmapData bmData = finalColorBitmap.LockBits(
				new System.Drawing.Rectangle(0, 0, Width, Height),
				System.Drawing.Imaging.ImageLockMode.ReadWrite, pf);
			int stride = bmData.Stride;
			System.IntPtr Scan0 = bmData.Scan0;
			unsafe
			{
				byte* p = (byte*)(void*)Scan0;
				int nOffset = stride - Width * 3;
				if (UseAlpha) nOffset = stride - Width * 4;
				for (int y = 0; y < Height; ++y)
				{
					for (int x = 0; x < Width; ++x)
					{
						//float4 lookup = GetPixel4(x, y);
						float p0 = m_color[x, y, 0];
						float p1 = m_color[x, y, 1];
						float p2 = m_color[x, y, 2];
						//lookup = lookup.Clamp(0.0f, 255.0f);
						p0 = System.Math.Min(255, System.Math.Max(0, p0));
						p1 = System.Math.Min(255, System.Math.Max(0, p1));
						p2 = System.Math.Min(255, System.Math.Max(0, p2));
						p[2] = (byte)(p0);
						p[1] = (byte)(p1);
						p[0] = (byte)(p2);
						p += 3;
						//if (UseAlpha)
						//{
						//    p[0] = (byte)lookup.w;
						//    p++;
						//}

					}
					p += nOffset;
				}
			}
			finalColorBitmap.UnlockBits(bmData);

			return finalColorBitmap;
		}

		// -----------------------------------------------------------------------------

		public void FileLoad(string filename)
		{
			System.Drawing.Image image = null;
			try
			{
				image = System.Drawing.Image.FromFile(filename);
			}
			catch
			{
				throw new Exception("Error loading file");
				//Console.WriteLine("Error loading");
				//return;
			}
			System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(image);
			ResetSize(image.Width, image.Height);
			//for (int y = 0; y < target.Height; y++)
			//    for (int x = 0; x < target.Width; x++)
			//    {
			//        target.SetPixel(x, y, new float3(
			//            bmp.GetPixel(x, y).R / 255.0f, bmp.GetPixel(x, y).G / 255.0f, bmp.GetPixel(x, y).B / 255.0f));
			//    }

			// GDI+ still lies to us - the return format is BGR, NOT RGB.
			System.Drawing.Imaging.BitmapData bmData = bmp.LockBits(
				new System.Drawing.Rectangle(0, 0, Width, Height),
				System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);//.Format24bppRgb);
			int stride = bmData.Stride;
			System.IntPtr Scan0 = bmData.Scan0;
			unsafe
			{
				byte* p = (byte*)(void*)Scan0;
				int nOffset = stride - Width * 4;
				for (int y = 0; y < Height; ++y)
				{
					for (int x = 0; x < Width; ++x)
					{
						float4 pix = new float4();
						pix.x = p[2];
						pix.y = p[1];
						pix.z = p[0];
						pix.w = p[3];
						SetPixel4(x, y, pix);
						p += 4;
					}
					p += nOffset;
				}
			}
			bmp.UnlockBits(bmData);
			bmp.Dispose();
			image.Dispose();
		}

		public void FileSave(string filename)
		{
			System.Drawing.Bitmap bmp = Draw(true);
			try
			{
				bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Tiff);//.Bmp);
			}
			catch
			{
				Console.WriteLine("Error saving.");
			}
		}

		public float[] Histogram()
		{
			float[] hist = new float[256];
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float3 pix1 = GetPixel3(x, y);
					float pix = pix1.Dot(new float3(0.3f, 0.5f, 0.2f));
					int index = (int)pix;
					if (index >= 256) index = 255;
					if (index < 0) index = 0;
					hist[index]++;
				}
			}
			return hist;
		}

/*		public float[] HistogramLine(Line l1)
		{
			const int size = 32;
			float[] hist = new float[size];
			Point diff = new Point(l1.p2.x- l1.p1.x, l1.p2.y - l1.p1.y);
			int dist = (int)Math.Sqrt(diff.X * diff.X + diff.Y * diff.Y);
			float t = 0.0f;
			int y = 0;
			//for (int y = -2; y < 3; y += 2) error with incrementing t variable too much
			{
				for (int count = 0; count <= dist; count++)
				{
					float fx = diff.X * t + l1.p1.x;
					float fy = diff.Y * t + l1.p1.y;
					float3 pix1 = GetPixel3((int)fx, (int)fy + y);
					float pix = pix1.Dot(new float3(0.3f, 0.5f, 0.2f));
					int index = (int)(pix * (size/256.0f));
					if (index >= size) index = size-1;
					if (index < 0) index = 0;
					hist[index]++;

					t += 1.0f / dist;
				}
			}
			return hist;
		}*/

		public float[] DifferenceHistogram()
		{
			float[] hist = new float[256];
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width-2; x++)
				{
					float3 pix1 = GetPixel3(x, y);
					float3 pix2 = GetPixel3(x+2, y);
					float p1 = pix1.Dot(new float3(0.3f, 0.5f, 0.2f));
					float p2 = pix2.Dot(new float3(0.3f, 0.5f, 0.2f));
					int index = System.Math.Abs((int)(p1 - p2));
					if (index >= 256) index = 255;
					if (index < 0) index = 0;
					hist[index]++;
				}
			}
			return hist;
		}

		public void MedianFilter(ref BitmapFloat4 target)
		{
			target.ResetSize(Width, Height);
//			target.Height = Height;
//			target.Width = Width;
//			target.m_color = new float[Width, Height, 4];

			int kernelWidth = 3;
			//int kernelHeight = 3;
			int kOffset = (kernelWidth - 1) / 2;
			if (kOffset != (((float)kernelWidth - 1.0f) / 2.0f)) throw new Exception("kernel size must be (even number) + 1.");
			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					float3 min = new float3(1110, 1110, 1110);
					for (int kX = -kOffset; kX <= kOffset; kX++)
					{
						for (int kY = -kOffset; kY <= kOffset; kY++)
						{
							float3 pix = GetPixel3(x + kX, y + kY);
							min.x = System.Math.Min(pix.x, min.x);
							min.y = System.Math.Min(pix.y, min.y);
							min.z = System.Math.Min(pix.z, min.z);
						}
					}
					target.SetPixel3(x, y, min);
				}
			}
		}

		public void BlackAndWhiteFilter(ref BitmapFloat4 target)
		{
			target.Height = Height;
			target.Width = Width;
			target.m_color = new float[Width, Height, 4];

			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					float3 pix1 = GetPixel3(x, y);
					float pix = pix1.Dot(new float3(0.3f, 0.5f, 0.2f));
					target.SetPixel3(x, y, new float3(pix, pix, pix));
				}
			}
		}

		public void LevelsFilterWithClamp(ref BitmapFloat4 target, float min, float max)
		{
			target.Height = Height;
			target.Width = Width;
			target.m_color = new float[Width, Height, 4];

			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					float3 pix1 = GetPixel3(x, y);
					pix1 = (pix1 - new float3(min,min,min)) / (max-min);
					pix1 *= 256.0f;
					pix1 = pix1.Min(new float3(256, 256, 256));
					pix1 = pix1.Max(new float3(0, 0, 0));
					target.SetPixel3(x, y, pix1);
				}
			}
		}

/*		public Box2 FloodFillAlongLine(Line l1, float threshold, float sign, float id, ref List<Box2> allLetterBoxes)
		{
			allLetterBoxes = new List<Box2>();
			Box2 totalBound = new Box2();
			Point diff = new Point(l1.p2.x - l1.p1.x, l1.p2.y - l1.p1.y);
			int dist = (int)Math.Sqrt(diff.X * diff.X + diff.Y * diff.Y);
			float t = 0.0f;
			for (int count = 0; count <= dist; count++)
			{
				float fx = diff.X * t + l1.p1.x;
				float fy = diff.Y * t + l1.p1.y;
				//SetPixel((int)fx, (int)fy + 100, new float3(0, 1, 255));
				Box2 bound = FloodFill(new float2(fx, fy), threshold, sign, id);
				if (bound.IsDefined())
				{
					//bound.Min -= new float2(0.5f, 0.5f);
					//bound.Max += new float2(1.5f, 1.5f);
					allLetterBoxes.Add(bound);
					totalBound.Union(bound);
				}

				t += 1.0f / dist;
			}
			return totalBound;
		}*/

		public Box2 FloodFill(int2 p1, float threshold, float sign, float id)
		{
			List<int2> fillList = new List<int2>();

			Box2 bounds = new Box2();
			int2 current = p1;
			bool done = false;
			do
			{
				float4 pix = GetPixel4((int)current.x, (int)current.y);
				int x = current.x;
				//while (((pix.x * sign) < (threshold * sign)) && (pix.w != id))
				while (pix.w != id)
				{
					bounds.MergePoint(new float2(x, current.y));
					fillList.Add(new int2(x + 1, current.y - 1));
					fillList.Add(new int2(x - 1, current.y - 1));
					fillList.Add(new int2(x, current.y - 1));
					fillList.Add(new int2(x + 1, current.y + 1));
					fillList.Add(new int2(x - 1, current.y + 1));
					fillList.Add(new int2(x, current.y + 1));
					SetPixel4((int)x, (int)current.y, new float4(pix.x, pix.y, 255, id));
					x += 1;
					pix = GetPixel4((int)x, (int)current.y);
				}
				x = current.x - 1;
				pix = GetPixel4((int)x, (int)current.y);
//				while (((pix.x * sign) < (threshold * sign)) && (pix.w != id))
				while (pix.w != id)
				{
					bounds.MergePoint(new float2(x, current.y));
					fillList.Add(new int2(x + 1, current.y - 1));
					fillList.Add(new int2(x - 1, current.y - 1));
					fillList.Add(new int2(x, current.y - 1));
					fillList.Add(new int2(x + 1, current.y + 1));
					fillList.Add(new int2(x - 1, current.y + 1));
					fillList.Add(new int2(x, current.y + 1));
					SetPixel4((int)x, (int)current.y, new float4(pix.x, pix.y, 255, id));
					x -= 1;
					pix = GetPixel4((int)x, (int)current.y);
				}
				if (fillList.Count > 0)
				{
					current = fillList[fillList.Count - 1];
					fillList.RemoveAt(fillList.Count - 1);
					while (!IsInBounds((int)current.x, (int)current.y))
					{
						current = fillList[fillList.Count - 1];
						fillList.RemoveAt(fillList.Count - 1);
					}
				}
				else done = true;
			} while (!done);
			return bounds;
		}

		public BitmapFloat4 SegmentToBitmap(int2 p1, float id)
		{
			List<int2> fillList = new List<int2>();
			List<int2> allPixels = new List<int2>();

			BoxI2 bounds = new BoxI2();
			int2 current = p1;
			bool done = false;
			do
			{
				float4 pix = GetPixel4((int)current.x, (int)current.y);
				int x = current.x;
				while (pix.w != id)
				{
					bounds.MergePoint(new int2(x, current.y));
					fillList.Add(new int2(x + 1, current.y - 1));
					fillList.Add(new int2(x - 1, current.y - 1));
					fillList.Add(new int2(x, current.y - 1));
					fillList.Add(new int2(x + 1, current.y + 1));
					fillList.Add(new int2(x - 1, current.y + 1));
					fillList.Add(new int2(x, current.y + 1));
					SetPixel4((int)x, (int)current.y, new float4(pix.x, pix.y, pix.z, id));
					allPixels.Add(new int2(x, current.y));
					x += 1;
					pix = GetPixel4((int)x, (int)current.y);
				}
				x = current.x - 1;
				pix = GetPixel4((int)x, (int)current.y);
				while (pix.w != id)
				{
					bounds.MergePoint(new int2(x, current.y));
					fillList.Add(new int2(x + 1, current.y - 1));
					fillList.Add(new int2(x - 1, current.y - 1));
					fillList.Add(new int2(x, current.y - 1));
					fillList.Add(new int2(x + 1, current.y + 1));
					fillList.Add(new int2(x - 1, current.y + 1));
					fillList.Add(new int2(x, current.y + 1));
					SetPixel4((int)x, (int)current.y, new float4(pix.x, pix.y, pix.z, id));
					allPixels.Add(new int2(x, current.y));
					x -= 1;
					pix = GetPixel4((int)x, (int)current.y);
				}
				if (fillList.Count > 0)
				{
					current = fillList[fillList.Count - 1];
					fillList.RemoveAt(fillList.Count - 1);
					while ((!IsInBounds((int)current.x, (int)current.y)) && (fillList.Count > 0) )
					{
						current = fillList[fillList.Count - 1];
						fillList.RemoveAt(fillList.Count - 1);
					}
				}
				else done = true;
			} while (!done);

			if (!bounds.IsDefined()) return null;
			BitmapFloat4 piece = new BitmapFloat4(bounds.Size().x + 3, bounds.Size().y + 3);
			foreach (var pos in allPixels)
			{
				float4 pix = GetPixel4(pos.x, pos.y);
				piece.SetPixel4(pos.x - bounds.m_min.x + 1, pos.y - bounds.m_min.y + 1, new float4(pix.x, pix.y, pix.z, 255.0f));
			}
			return piece;
		}

		// returns true if vertical line has stuff in it, false if it's white
		public bool ScanVerticalLine(int x)
		{
			for (int y = 0; y < Height; y++)
			{
				float3 pix1 = GetPixel3(x, y);
				//if (pix1 != new float3(255, 255, 255)) return true;
				if (pix1.x < 128.0f) return true;
			}
			return false;
		}

		// returns true if line has stuff in it, false if it's white
		public bool ScanHorizontalLine(BoxI2 box, int y)
		{
			for (int x = box.m_min.x; x <= box.m_max.x; x++)
			{
				float3 pix1 = GetPixel3(x, y);
				//if (pix1 != new float3(255, 255, 255)) return true;
				if (pix1.x < 128.0f) return true;
			}
			return false;
		}

		public float CompareRectangles(BoxI2 boxA, BitmapFloat4 alphabetFile, BoxI2 boxB, float currentMinDiff, float2 italic)
		{
			float2 sizeA = new float2(boxA.Size());
			float2 sizeB = new float2(boxB.Size());
			float ratioA = sizeA.x / sizeA.y;
			if (ratioA < 1.0f) ratioA = 1.0f / ratioA;

			float ratioB = sizeB.x / sizeB.y;
			if (ratioB < 1.0f) ratioB = 1.0f / ratioB;

			float total = 0;
			float2 boxASize = new float2(boxA.Max - boxA.Min);
			float2 boxBSize = new float2(boxB.Max - boxB.Min);
			float xInc = 1.0f / boxASize.x;
			if (boxASize.x < 8.0f) xInc = 1.0f / 16.0f;	// Not enough sampling for thin letters like "l"
//			float italic = 0.0f;
			float yInc = 1.0f / 16.0f;
			xInc = 1.0f / 16.0f;
			for (float y = 0.0f; y <= (1.0f + 1.0f / boxASize.y); y += yInc)// 1.0f / boxASize.y)
			{
				float bestTempTotal = float.MaxValue;
				float bestJitter = 0.0f;
				for (float jitter = 0; jitter <= 0; jitter++)
				{
					float tempTotal = 0.0f;
					for (float x = 0.0f; x <= (1.0f + 1.0f / boxASize.x); x += xInc)
					{
						float4 pixB = alphabetFile.GetPixelBilinear4(new float2(x * boxBSize.x + boxB.Min.x, y * boxBSize.y + boxB.Min.y));
						float2 pos = new float2(x * boxASize.x + boxA.Min.x + italic.x * y * boxASize.y, y * boxASize.y + boxA.Min.y);
						float4 pixA = GetPixelBilinear4(pos);
						float diff = pixA.y - pixB.y;
						//pixA = GetPixelBilinear4(pos + new float2(xInc, 0));
						//if (pixA.y - pixB.y < diff)
						//{
						//}
						//diff = Math.Min(diff, pixA.y - pixB.y);
						//pixA = GetPixelBilinear4(pos + new float2(-xInc, 0));
						//diff = Math.Min(diff, pixA.y - pixB.y);

						float diffSquared = diff * diff * diff * diff;
						tempTotal += diffSquared;
						//if (total > currentMinDiff) return total;
						//x += 1.0f / boxASize.x;
					}
					if (tempTotal < bestTempTotal)
					{
						bestTempTotal = tempTotal;
						bestJitter = jitter;
					}
				}
				total += bestTempTotal;
				//italic += bestJitter;
				//y += 1.0f / boxASize.y;
			}
			return total;// *(Math.Max(Math.Abs(ratioA - ratioB), 1.0f));
		}

		public BitmapFloat4 OverlayRectangle(BoxI2 boxA, BitmapFloat4 alphabetFile, BoxI2 boxB, float2 italic)
		{
			//Bitmap result = new Bitmap((int)(boxA.Max.x - boxA.Min.x) + 1, (int)(boxA.Max.y - boxA.Min.y) + 1);
			BitmapFloat4 result = new BitmapFloat4(32, 32);
			float2 boxASize = new float2(boxA.Max - boxA.Min);
			float2 boxBSize = new float2(boxB.Max - boxB.Min);
			float xInc = 1.0f / boxASize.x;
			if (boxASize.x < 8.0f) xInc = 1.0f / 8.0f;	// Not enough sampling for thin letters like "l"
			float yInc = 1.0f / 32.0f;
			xInc = 1.0f / 32.0f;
			int yIndex = 0;
			for (float y = 0.0f; y <= (1.0f + 1.0f / boxASize.y); y += yInc)// 1.0f / boxASize.y)
			{
				int xIndex = 0;
				for (float x = 0.0f; x <= (1.0f + 1.0f / boxASize.x); x += xInc)
				{
					//float2 pos = new float2(x * boxASize.x + boxA.Min.x + italic.x * y * boxASize.y, y * boxASize.y + boxA.Min.y);
					float2 pos = new float2(x * boxASize.x + boxA.Min.x, y * boxASize.y + boxA.Min.y);
					float4 pixA = GetPixelBilinear4(pos);
					float4 pixB = alphabetFile.GetPixelBilinear4(new float2(x * boxBSize.x + boxB.Min.x, y * boxBSize.y + boxB.Min.y));
					float diff = pixA.y - pixB.y;
					float diffSquared = System.Math.Abs(diff);// *diff;

					SetPixel3((int)System.Math.Round(x * boxASize.x + boxA.Min.x), (int)System.Math.Round(y * boxASize.y + boxA.Min.y) - 32,
						new float3(pixA.y, pixA.y, 0));
					SetPixel3((int)System.Math.Round(x * boxASize.x + boxA.Min.x), (int)System.Math.Round(y * boxASize.y + boxA.Min.y) - 64,
						new float3(diffSquared, diffSquared, diffSquared));
					result.SetPixel3(xIndex, yIndex, new float3(diffSquared, diffSquared, diffSquared));
					xIndex++;
				}
				yIndex++;
			}
			return result;
		}

		public void EdgeDetectMe(BoxI2 boxA, ref List<float2> allEdges)
		{
			float2 boxASize = new float2(boxA.Max - boxA.Min);
			float xInc = 1.0f / boxASize.x;
			float yInc = 1.0f / boxASize.y;
			for (float y = 0.0f; y <= (1.0f + 1.0f / boxASize.y); y += yInc)
			{
				for (float x = 0.0f; x <= (1.0f + 1.0f / boxASize.x); x += xInc)
				{
					float2 pos = new float2(x * boxASize.x + boxA.Min.x, y * boxASize.y + boxA.Min.y);
					float pix00 = GetPixelBilinear4(pos).y;
					float pix10 = GetPixelBilinear4(pos + new float2(xInc,0) * boxASize).y;
					float pix01 = GetPixelBilinear4(pos + new float2(0, yInc) * boxASize).y;
					float pix11 = GetPixelBilinear4(pos + new float2(xInc, yInc) * boxASize).y;
					if ( ((pix00 < 128) && (pix01 < 128) && (pix10 >= 128) && (pix11 >= 128)) ||
						 ((pix00 >= 128) && (pix01 >= 128) && (pix10 < 128) && (pix11 < 128)) )
					{
						float zeroCrossing0 = (128 - pix00) / (pix10 - pix00);
						float zeroCrossing1 = (128 - pix01) / (pix11 - pix01);
						float xDelta = zeroCrossing1 - zeroCrossing0;
						if (System.Math.Abs(xDelta) > 0.4f) continue;	// threshold to not over-do it.
						allEdges.Add(new float2(xDelta, 1.0f));
						//SetPixel((int)pos.x, (int)pos.y, new float3(255, 0, 0));
					}
				}
			}
		}

		bool IsGreen(float3 pix)
		{
			if ((pix.x < 16) && (pix.z < 16) && (pix.y > 64)) return true;
			return false;
		}

		public void ErasePink(ref BitmapFloat4 target)
		{
			target.Height = Height;
			target.Width = Width;
			target.m_color = new float[Width, Height, 4];

			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					float3 pix1 = GetPixel3(x, y);
					//float3 pixUp = GetPixel3(x, y - 1);
					//float3 pixDown = GetPixel3(x, y + 1);
					//float3 pixRight = GetPixel3(x + 1, y);
					//float3 pixLeft = GetPixel3(x - 1, y);
					float4 pix = new float4(pix1.x, pix1.y, pix1.z, 255.0f);
					// Magic number for puzzle #2 green hack
					//if ((!IsGreen(pixUp)) && (!IsGreen(pixDown)) && (!IsGreen(pixRight)) && (!IsGreen(pixLeft)))
					//{
					    if ((pix.x > pix.y * 1.6f) && (pix.x > pix.z) && (pix.x > 128)) pix.w = 0;
					//}
					target.SetPixel4(x, y, pix);
				}
			}
		}

		public void DistanceFromColorFilter(ref BitmapFloat4 target, float3 col, float multiplier)
		{
			target.Height = Height;
			target.Width = Width;
			target.m_color = new float[Width, Height, 4];

			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					float3 pix1 = GetPixel3(x, y);
					float dist = pix1.Distance(col)*multiplier;
					float4 pix = new float4(dist, dist, dist, 255.0f);
					target.SetPixel4(x, y, pix);
				}
			}
		}

        public void DistanceFromRedFilter(ref BitmapFloat4 target, float3 col, float multiplierRed, float multiplierBlack)
        {
            target.Height = Height;
            target.Width = Width;
            target.m_color = new float[Width, Height, 4];

            float3 black = new float3(0, 0, 0);

            for (int y = 0; y < target.Height; y++)
            {
                for (int x = 0; x < target.Width; x++)
                {
                    float4 pix1 = GetPixel4(x, y);
                    float3 pix3 = GetPixel3(x, y);
                    if (pix1.w > 0.0)
                    {
                        float distRed = (pix1.x - col.x) * multiplierRed;

                        float distBlack = pix3.Distance(black);

                        float dist;

                        if (distBlack < 150)
                        {
                            dist = 255.0f;
                        }
                        else
                        {
                            dist = distRed;
                        }

                        if (dist > 150)
                        {
                            dist = 255;
                        }
                        else
                        {
                            dist = 0;
                        }


                        float4 pix = new float4(dist, dist, dist, 255.0f);
                        target.SetPixel4(x, y, pix);

                    }
                    else
                    {
                        float4 pix = new float4(0, 0, 0, 0.0f);
                        target.SetPixel4(x, y, pix);
                    }
                }
            }
        }



		public void Rotate180(ref BitmapFloat4 target)
		{
			target.Height = Height;
			target.Width = Width;
			target.m_color = new float[Width, Height, 4];

			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					float4 pix = GetPixel4(x, y);
					target.SetPixel4((Width - 1) - x, (Height - 1) - y, pix);
				}
			}
		}

		public void ExpandCanvas(ref BitmapFloat4 target, int2 size)
		{
			target.ResetSize(Width + size.x * 2, Height + size.y * 2);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float4 pix = GetPixel4(x, y);
					target.SetPixel4(x + size.x, y + size.y, pix);
				}
			}
		}

		public BoxI2 Rotate(ref BitmapFloat4 target, float angle)
		{
			BoxI2 bounds = new BoxI2();
			//ExpandCanvas(ref target, new int2(Height, Width));
			//target.ResetSize(Height + Width, Height + Width);
			target.ResetSize(Width, Height);

			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					// rotate around centroid.
					float2 trans = new float2(x - Width / 2, y - Height / 2).Rotate(angle);
					trans.x += Width / 2;
					trans.y += Height / 2;
					float4 pix = GetPixelBilinear4(trans);
					//float t = 100.0f;
					//pix.x = pix.x > t ? 255 : 0;
					//pix.y = pix.y > t ? 255 : 0;
					//pix.z = pix.z > t ? 255 : 0;
					target.SetPixel4(x, y, pix);
					if (pix.w > 0.0f) bounds.MergePoint(new int2(x, y));
				}
			}
			BitmapFloat4 smaller = new BitmapFloat4(bounds.Size().x + 1, bounds.Size().y + 1);
			for (int y = 0; y < smaller.Height; y++)
			{
				for (int x = 0; x < smaller.Width; x++)
				{
					float4 pix = target.GetPixel4(x + bounds.Min.x, y + bounds.Min.y);
					smaller.SetPixel4(x, y, pix);
				}
			}
			target = smaller;
			return new BoxI2(new int2(0, 0), new int2(smaller.Width, smaller.Height));
		}

		// ------------------ Color conversion functions ------------
		// http://www.easyrgb.com/math.php?MATH=M19#text19
		// http://www.cs.rit.edu/~ncs/color/t_convert.html - color conversion algorithm.
		// http://en.wikipedia.org/wiki/HSL_color_space

		public static void RGB2HSV(BitmapFloat4 target, BitmapFloat4 source1)
		{
			//target = new Bitmap(source1.Width, source1.Height);
			for (int y = 0; y < target.Height; y++)
				for (int x = 0; x < target.Width; x++)
				{
					float4 pix = source1.GetPixel4(x, y);

					float3 workingColor = new float3(pix.x, pix.y, pix.z);
					float h, s, v;
					float minColor, maxColor, delta;
					minColor = System.Math.Min(System.Math.Min(workingColor.x, workingColor.y), workingColor.z);
					maxColor = System.Math.Max(System.Math.Max(workingColor.x, workingColor.y), workingColor.z);
					v = maxColor;				// v
					delta = maxColor - minColor;
					if (delta != 0)		// This is different than the algorithm on the net. That one was buggy.
					{
						s = delta / maxColor;		// s

						if (workingColor.x == maxColor)
							h = (workingColor.y - workingColor.z) / delta;		// between yellow & magenta
						else if (workingColor.y == maxColor)
							h = 2 + (workingColor.z - workingColor.x) / delta;	// between cyan & yellow
						else
							h = 4 + (workingColor.x - workingColor.y) / delta;	// between magenta & cyan

						h *= 60;				// degrees
						if (h < 0)
							h += 360;
					}
					else
					{
						// r = g = b = 0		// s = 0, v is undefined
						s = 0;
						h = 0;
					}
					h /= 360.0f;
					pix = new float4(h, s, v, pix.w);

					target.SetPixel4(x, y, pix);
				}
		}

		private static void HSV2RGB(BitmapFloat4 target, BitmapFloat4 source1)
		{
			target = new BitmapFloat4(source1.Width, source1.Height);
			for (int y = 0; y < target.Height; y++)
				for (int x = 0; x < target.Width; x++)
				{
					float4 hsv = source1.GetPixel4(x, y);

					float3 workingColor;
					float h = hsv.x * 360.0f;
					float s = hsv.y;
					float v = hsv.z;
					float i;
					float f, p, q, t;
					if (s == 0)
					{
						// achromatic (grey)
						workingColor.x = workingColor.y = workingColor.z = v;
					}
					else
					{
						h /= 60.0f;			// sector 0 to 5
						i = (float)System.Math.Floor(h);
						f = h - i;			// factorial part of h
						p = v * (1 - s);
						q = v * (1 - s * f);
						t = v * (1 - s * (1 - f));
						if (i == 0)
						{
							workingColor.x = v;
							workingColor.y = t;
							workingColor.z = p;
						}
						else if (i == 1)
						{
							workingColor.x = q;
							workingColor.y = v;
							workingColor.z = p;
						}
						else if (i == 2)
						{
							workingColor.x = p;
							workingColor.y = v;
							workingColor.z = t;
						}
						else if (i == 3)
						{
							workingColor.x = p;
							workingColor.y = q;
							workingColor.z = v;
						}
						else if (i == 4)
						{
							workingColor.x = t;
							workingColor.y = p;
							workingColor.z = v;
						}
						else
						{
							workingColor.x = v;
							workingColor.y = p;
							workingColor.z = q;
						}
					}

					target.SetPixel4(x, y, new float4(workingColor.x, workingColor.y, workingColor.z, hsv.w));
				}
		}

		public BitmapFloat4 ScaleUpInteger(int scale)
		{
			BitmapFloat4 target = new BitmapFloat4(Width * scale, Height * scale);
			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					float4 pix = GetPixel4(x / scale, y / scale);
					target.SetPixel4(x, y, pix);
				}
			}
			return target;
		}

		public void Accumulate(BitmapFloat4 other)
		{
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float4 p0 = other.GetPixel4(x, y);
					float4 p1 = GetPixel4(x, y);
					SetPixel4(x, y, p0 + p1);
				}
			}
		}

		public BitmapFloat4 Mul(float scale)
		{
			BitmapFloat4 target = new BitmapFloat4(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float4 p0 = GetPixel4(x, y) * scale;
					target.SetPixel4(x, y, p0);
				}
			}
			return target;
		}

		public BitmapFloat4 PowXYZ(float exp)
		{
			BitmapFloat4 target = new BitmapFloat4(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float4 p0 = GetPixel4(x, y);
					p0.x = (float)System.Math.Pow(p0.x, exp);
					p0.y = (float)System.Math.Pow(p0.y, exp);
					p0.z = (float)System.Math.Pow(p0.z, exp);
					target.SetPixel4(x, y, p0);
				}
			}
			return target;
		}

		public BitmapFloat4 NormalizeToMaxPixel()
		{
			BitmapFloat4 target = new BitmapFloat4(Width, Height);
			float max = -float.MaxValue;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float4 p0 = GetPixel4(x, y);
					max = System.Math.Max(max, p0.x);
					max = System.Math.Max(max, p0.y);
					max = System.Math.Max(max, p0.z);
				}
			}
			float inv = 1.0f / max;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float4 p0 = GetPixel4(x, y) * inv;
					target.SetPixel4(x, y, p0);
				}
			}
			return target;
		}

		public BitmapFloat4 FourierLine(int yIndex)
		{
			int bigWidth = 128;
			BitmapFloat4 result = new BitmapFloat4(bigWidth, 1);
			for (int freq = 0; freq < bigWidth; freq++)
			{
				float total = 0;
				float totalCos = 0;
				float totalSin = 0;
				for (int x = 0; x < bigWidth; x++)
				{
					float4 pix = GetPixel4(x, yIndex);
					float val = pix.z;
					float angle = ((float)System.Math.PI * 2) * x * freq / ((float)bigWidth);
					float sin = (float)System.Math.Sin(angle);
					float cos = (float)System.Math.Cos(angle);
					if ((pix.w == 255) && (pix.xyz.Len() > 0))
					{
						total++;
						totalCos += val * cos;
						totalSin += val * sin;
					}
				}
				if (total > 0)
				{
					totalSin /= total;
					totalCos /= total;
					float accum = (System.Math.Abs(totalCos) + System.Math.Abs(totalSin));
					result.SetPixel4(freq, 0, new float4(totalSin, totalCos, accum, 255));
				}
			}
			return result;
		}

		public BitmapFloat4 InverseFourierLine()
		{
			int bigWidth = 128;
			BitmapFloat4 result = new BitmapFloat4(bigWidth, 1);
			for (int freq = 0; freq < bigWidth/2; freq++)
			{
				float4 pix = GetPixel4(freq, 0);
				float sinPart = pix.x;
				float cosPart = pix.y;
				for (int x = 0; x < bigWidth; x++)
				{
					float angle = ((float)System.Math.PI * 2) * x * freq / ((float)bigWidth);
					float sin = (float)System.Math.Sin(angle);
					float cos = (float)System.Math.Cos(angle);
					float final = sin * sinPart + cos * cosPart;
					result.AddPixel4(x, 0, new float4(final, final, final, 0));
				}
			}
			return result;
		}

	}
}

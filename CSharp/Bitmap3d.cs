﻿#region ApacheLicense2.0
// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion
#define BITMAP3D

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Otavio.Math;

namespace Otavio.Image
{
	public sealed class Bitmap3d
	{
		private int m_width, m_height, m_depth;
		public int Width
		{
			get { return m_width; }
			set { m_width = value; }
		}
		public int Height
		{
			get { return m_height; }
			set { m_height = value; }
		}
		public int Depth
		{
			get { return m_depth; }
			set { m_depth = value; }
		}

		public byte[] m_color;

		public Bitmap3d()
		{
			ResetSize(8, 8, 8);
		}

		public Bitmap3d(int width, int height, int depth)
		{
			ResetSize(width, height, depth);
		}

		public Bitmap3d(Bitmap3d orig)
		{
			ResetSize(orig.Width, orig.Height, orig.Depth);
			Array.Copy(orig.m_color, m_color, m_color.Length);
		}

		public void ResetSize(int w, int h, int d)
		{
			Height = h;
			Width = w;
			Depth = d;
			m_color = new byte[Width * Height * Depth];
		}

		bool IsInBounds(int x, int y, int z)
		{
			if (x < 0) return false;
			if (y < 0) return false;
			if (z < 0) return false;
			if (x >= Width) return false;
			if (y >= Height) return false;
			if (z >= Depth) return false;
			return true;
		}

		public byte GetPixel(int x, int y, int z)
		{
			return m_color[x + y * Width + z * Width * Height];
		}

		public float GetPixelBilinear(float x, float y, float z)
		{
			int floorX = (int)System.Math.Floor(x);
			int floorY = (int)System.Math.Floor(y);
			int floorZ = (int)System.Math.Floor(z);
			float remainderX = x - (float)floorX;
			float remainderY = y - (float)floorY;
			float remainderZ = z - (float)floorZ;
			float pix000 = GetPixel(floorX, floorY, floorZ);
			float pix100 = GetPixel(floorX + 1, floorY, floorZ);
			float pix010 = GetPixel(floorX, floorY + 1, floorZ);
			float pix110 = GetPixel(floorX + 1, floorY + 1, floorZ);
			float pix001 = GetPixel(floorX, floorY, floorZ + 1);
			float pix101 = GetPixel(floorX + 1, floorY, floorZ + 1);
			float pix011 = GetPixel(floorX, floorY + 1, floorZ + 1);
			float pix111 = GetPixel(floorX + 1, floorY + 1, floorZ + 1);

			float pixX00 = pix100 * remainderX + pix000 * (1.0f - remainderX);
			float pixX10 = pix110 * remainderX + pix010 * (1.0f - remainderX);
			float pixX01 = pix101 * remainderX + pix001 * (1.0f - remainderX);
			float pixX11 = pix111 * remainderX + pix011 * (1.0f - remainderX);
			float pixY0 = pixX10 * remainderY + pixX00 * (1.0f - remainderY);
			float pixY1 = pixX11 * remainderY + pixX01 * (1.0f - remainderY);
			float pixZ = pixY1 * remainderZ + pixY0 * (1.0f - remainderZ);
			return pixZ;
		}

		public void SetPixel(int x, int y, int z, byte val)
		{
			m_color[x + y * Width + z * Width * Height] = val;
		}

		public void InitLayer(int layer, BitmapGray source, int sourceX, int sourceY)
		{
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					SetPixel(x, y, layer, source.GetPixel(x + sourceX, y + sourceY));
				}
			}
		}

		public BitmapGray GetLayer(int layer)
		{
			BitmapGray bg = new BitmapGray(Width, Height);
			Array.Copy(m_color, layer * Width * Height, bg.m_color, 0, Width * Height);
			return bg;
		}

		public System.Drawing.Bitmap DrawLayer(int layer)
		{
			if (m_color == null) return null;

			System.Drawing.Bitmap finalColorBitmap = new System.Drawing.Bitmap(Width, Height);
			System.Drawing.Imaging.PixelFormat pf = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
			System.Drawing.Imaging.BitmapData bmData = finalColorBitmap.LockBits( new System.Drawing.Rectangle(0, 0, Width, Height),
				System.Drawing.Imaging.ImageLockMode.WriteOnly, pf);
			int stride = bmData.Stride;
			System.IntPtr Scan0 = bmData.Scan0;
			unsafe
			{
				byte* p = (byte*)(void*)Scan0;
				int nOffset = stride - Width * 4;
				for (int y = 0; y < Height; ++y)
				{
					for (int x = 0; x < Width; ++x)
					{
						byte lookup = GetPixel(x, y, layer);
						p[0] = lookup;
						p[1] = lookup;
						p[2] = lookup;
						p[3] = 0xff;
						p += 4;
					}
					p += nOffset;
				}
			}
			finalColorBitmap.UnlockBits(bmData);

			return finalColorBitmap;
		}

		public float3 CalcGrad(float3 pos)
		{
			float delta = 0.1f;
			float c0 = GetPixelBilinear(pos.x, pos.y, pos.z);
			float cx = GetPixelBilinear(pos.x + delta, pos.y, pos.z);
			float cy = GetPixelBilinear(pos.x, pos.y + delta, pos.z);
			float cz = GetPixelBilinear(pos.x, pos.y, pos.z + delta);
			float3 result = new float3(cx - c0, cy - c0, cz - c0) * (1.0f / delta);
			if (result.LengthSquared() > 0) result = result.Normalize();
			return result;
		}

		public float3 CalcGradFast(float3 pos)
		{
			int x = (int)pos.x;
			int y = (int)pos.y;
			int z = (int)pos.z;
			int delta = 1;
			int c0 = GetPixel(x, y, z);
			int cx = GetPixel(x + delta, y, z);
			int cy = GetPixel(x, y + delta, z);
			int cz = GetPixel(x, y, z + delta);
			float3 result = new float3(cx - c0, cy - c0, cz - c0);// / delta;
			float ls = result.LengthSquared();
			if (result.LengthSquared() > 0) result = result / (float)System.Math.Sqrt(ls);// result.Normalize();
			return result;
		}

		public void MarchingCubes(float threshold = 128)
		{
			Microscope.Mesh isoMesh = new Microscope.Mesh("IsoMesh");
			Microscope.gridcell gc = new Microscope.gridcell();
			for (int z = 0; z < Depth - 2; z++)
			{
				for (int y = 0; y < Height - 2; y++)
				{
					for (int x = 0; x < Width - 2; x++)
					{
						gc.p[0] = new float3(x, y, z);
						gc.p[1] = new float3(x + 1, y, z);
						gc.p[2] = new float3(x + 1, y + 1, z);
						gc.p[3] = new float3(x, y + 1, z);
						gc.p[4] = new float3(x, y, z + 1);
						gc.p[5] = new float3(x + 1, y, z + 1);
						gc.p[6] = new float3(x + 1, y + 1, z + 1);
						gc.p[7] = new float3(x, y + 1, z + 1);
						gc.val[0] = new float4(CalcGradFast(gc.p[0]), GetPixel(x, y, z));
						gc.val[1] = new float4(CalcGradFast(gc.p[1]), GetPixel(x + 1, y, z));
						gc.val[2] = new float4(CalcGradFast(gc.p[2]), GetPixel(x + 1, y + 1, z));
						gc.val[3] = new float4(CalcGradFast(gc.p[3]), GetPixel(x, y + 1, z));
						gc.val[4] = new float4(CalcGradFast(gc.p[4]), GetPixel(x, y, z + 1));
						gc.val[5] = new float4(CalcGradFast(gc.p[5]), GetPixel(x + 1, y, z + 1));
						gc.val[6] = new float4(CalcGradFast(gc.p[6]), GetPixel(x + 1, y + 1, z + 1));
						gc.val[7] = new float4(CalcGradFast(gc.p[7]), GetPixel(x, y + 1, z + 1));

						isoMesh.Polygonise(gc, threshold);
					}
				}
				Console.WriteLine("Slice: " + z + " / " + Depth);
			}
			isoMesh.GenerateDefaultFaceList();
			isoMesh.MakeVertsUnique();
			isoMesh.SaveOBJFile("test3.obj");
		}

	}
}

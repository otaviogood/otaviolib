﻿#region ApacheLicense2.0
// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion
#define BITMAPGRAY

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Otavio.Math;

namespace Otavio.Image
{
	public sealed class BitmapGray
	{
		private int m_width, m_height;
		public int Width
		{
			get { return m_width; }
			set { m_width = value; }
		}
		public int Height
		{
			get { return m_height; }
			set { m_height = value; }
		}

		public byte[] m_color;

		public BitmapGray()
		{
			ResetSize(8, 8);
		}

		public BitmapGray(int width, int height)
		{
			ResetSize(width, height);
		}

		public BitmapGray(List<bool> source, int width, int height)
		{
			ResetSize(width, height);
			int index = 0;
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					SetPixel(x, y, source[index] ? (byte)255 : (byte)0);
					index++;
				}
			}
		}

		public BitmapGray(System.Drawing.Bitmap source)
		{
			ResetSize(source.Width, source.Height);
			//for (int y = 0; y < source.Height; y++)
			//{
			//    for (int x = 0; x < source.Width; x++)
			//    {
			//        SetPixel(x, y, source.GetPixel(x, y).R);
			//    }
			//}

            // Lock the bitmap's bits.  
            Rectangle rect = new Rectangle(0, 0, source.Width, source.Height);
            System.Drawing.Imaging.BitmapData bmpData =
				source.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
				source.PixelFormat);
			int bpp = 4;
			if (source.PixelFormat == PixelFormat.Format24bppRgb) bpp = 3;

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
			int bytes = bmpData.Stride * source.Height;


			if (source.PixelFormat != PixelFormat.Format8bppIndexed)
			{
				byte[] rgbValues = new byte[bytes];
				// Copy the RGB values into the array.
				System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
				// Set every third value to 255. A 24bpp bitmap will look red.  
				int index = 0;
				unsafe	// lame optimziation attempt
				{
					unchecked	// lame optimziation attempt
					{
						int size = Width * Height;
						for (int counter = 0; counter < size; counter++)
						{
							//				if (counter >= Width * Height)
							//					Console.WriteLine("Error dude" + counter.ToString());
							m_color[counter] = rgbValues[index + 2];
							index += bpp;
						}
					}
				}
			}
			else
			{
				System.Runtime.InteropServices.Marshal.Copy(ptr, m_color, 0, bytes);
			}

            // Copy the RGB values back to the bitmap
//            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            source.UnlockBits(bmpData);

		}

#if BITMAPARGB
		public BitmapGray(BitmapARGB source, float3 colorVector)
		{
			ResetSize(source.Width, source.Height);
			for (int count = 0; count < Width * Height; count++)
			{
				m_color[count] = (byte)(((source.data[count] >> 16) & 0xff) * colorVector.x +
					((source.data[count] >> 8) & 0xff) * colorVector.y +
					(source.data[count] & 0xff) * colorVector.z);
			}
		}
#endif

#if BITMAP1BIT
		public BitmapGray(Bitmap1Bit source)
		{
			ResetSize(source.Width, source.Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					SetPixel(x, y, (byte)(source.GetPixel(x, y) * 255));
				}
			}
		}
#endif

		public BitmapGray(BitmapGray orig)
		{
			ResetSize(orig.Width, orig.Height);
			Array.Copy(orig.m_color, m_color, m_color.Length);
		}

		public void ResetSize(int w, int h)
		{
			Height = h;
			Width = w;
			m_color = new byte[Width * Height];
		}

        public void SetFromArray(byte[] source, int sourceOffset)
        {
            int size = Width * Height;
            Array.Copy(source, sourceOffset, m_color, 0, size);
        }

        bool IsInBounds(int x, int y)
		{
			if (x < 0) return false;
			if (y < 0) return false;
			if (x >= Width) return false;
			if (y >= Height) return false;
			return true;
		}

		public byte GetPixel(int x, int y)
		{
			//if (x >= Width) x = Width - 1;
			//if (x < 0) x = 0;
			//if (y >= Height) y = Height - 1;
			//if (y < 0) y = 0;
			return m_color[x + y * Width];
		}

		public byte GetPixelClamped(int x, int y)
		{
			if (x >= Width) x = Width - 1;
			if (x < 0) x = 0;
			if (y >= Height) y = Height - 1;
			if (y < 0) y = 0;
			return m_color[x + y * Width];
		}

		public int GetPixelBilinear(float x, float y)
		{
			float floorX = (float)System.Math.Floor(x);
			float floorY = (float)System.Math.Floor(y);
			float remainderX = x - floorX;
			float remainderY = y - floorY;
			float pix00 = GetPixel((int)floorX, (int)floorY);
			float pix10 = GetPixel((int)floorX + 1, (int)floorY);
			float pix01 = GetPixel((int)floorX, (int)floorY + 1);
			float pix11 = GetPixel((int)floorX + 1, (int)floorY + 1);

			float pixX0 = pix10 * remainderX + pix00 * (1.0f - remainderX);
			float pixX1 = pix11 * remainderX + pix01 * (1.0f - remainderX);
			float pixY = pixX1 * remainderY + pixX0 * (1.0f - remainderY);
			return (int)pixY;
		}

		public int GetPixelBilinearSafe(float x, float y)
		{
			if (x >= (Width - 1)) x = Width - 2;
			if (x < 0) x = 0;
			if (y >= (Height - 1)) y = Height - 2;
			if (y < 0) y = 0;
			return GetPixelBilinear(x, y);
		}

		public void SetPixel(int x, int y, byte val)
		{
			if (x >= Width) return;
			if (x < 0) return;
			if (y >= Height) return;
			if (y < 0) return;
			m_color[x + y * Width] = val;
		}

		public void AddPixel(int x, int y, byte val)
		{
			int temp = GetPixel(x, y);
			temp += val;
			if (temp > 255) temp = 255;
			SetPixel(x, y, (byte)temp);
		}

		public System.Drawing.Bitmap Draw(UInt32 color = 0xffffff)
		{
			if (m_color == null) return null;
			uint red = (color >> 16) & 0xff;
			uint green = (color >> 8) & 0xff;
			uint blue = color & 0xff;
            if (color == 0xffffff)      // grayscale version is faster - goes to indexed color grayscale.
            {
                System.Drawing.Imaging.PixelFormat pf = System.Drawing.Imaging.PixelFormat.Format8bppIndexed;
                System.Drawing.Bitmap finalColorBitmap = new System.Drawing.Bitmap(Width, Height, pf);
                System.Drawing.Imaging.BitmapData bmData = finalColorBitmap.LockBits(new System.Drawing.Rectangle(0, 0, Width, Height),
                    System.Drawing.Imaging.ImageLockMode.ReadWrite, pf);
                Marshal.Copy(m_color, 0, bmData.Scan0, Width * Height);
                finalColorBitmap.UnlockBits(bmData);

                ColorPalette pal = finalColorBitmap.Palette;
                for (int i = 0; i < pal.Entries.Length; i++) pal.Entries[i] = Color.FromArgb(i, i, i);
                finalColorBitmap.Palette = pal;
                return finalColorBitmap;
            }
            else
            {

                System.Drawing.Imaging.PixelFormat pf = System.Drawing.Imaging.PixelFormat.Format24bppRgb;
                System.Drawing.Bitmap finalColorBitmap = new System.Drawing.Bitmap(Width, Height);
                // GDI+ still lies to us - the return format is BGR, NOT RGB.
                System.Drawing.Imaging.BitmapData bmData = finalColorBitmap.LockBits(
                    new System.Drawing.Rectangle(0, 0, Width, Height),
                    System.Drawing.Imaging.ImageLockMode.ReadWrite, pf);
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    int nOffset = stride - Width * 3;
                    for (int y = 0; y < Height; ++y)
                    {
                        for (int x = 0; x < Width; ++x)
                        {
                            int lookup = GetPixel(x, y);
                            p[2] = (byte)((lookup * red) >> 8);
                            p[1] = (byte)((lookup * green) >> 8);
                            p[0] = (byte)((lookup * blue) >> 8);
                            p += 3;
                        }
                        p += nOffset;
                    }
                }
                finalColorBitmap.UnlockBits(bmData);
                return finalColorBitmap;
            }
		}

		public void FileSave(string filename)
		{
			System.Drawing.Bitmap bmp = Draw();
			try
			{
				bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
			}
			catch
			{
				Console.WriteLine("Error saving.");
			}
		}

		public static BitmapGray FileLoad(string filename)
		{
			System.Drawing.Bitmap bmp = (Bitmap)System.Drawing.Bitmap.FromFile(filename);
			BitmapGray bg = new BitmapGray(bmp);
			return bg;
		}

		public void FillCircle(float2 pos, float radius, byte color)
		{
			for (int y = (int)(pos.y - radius); y < (int)(pos.y + radius); y++)
			{
				for (int x = (int)(pos.x - radius); x < (int)(pos.x + radius); x++)
				{
					if (pos.Distance(new float2((float)x, (float)y)) <= radius)
						SetPixel(x, y, color);
				}
			}
		}

		public void DrawLine(float2 posA, float2 posB, byte color)
		{
			float len = posB.Distance(posA);
			for (float count = 0.0f; count <= 1.0f; count += 0.5f / len)
			{
				float2 pos = (posB - posA) * count + posA;
				SetPixel((int)pos.x, (int)pos.y, color);
			}
		}


		public void Compute4COMs(float2[] fourCorners)
		{
			// ---- Set up 4 corner COGs ----
			int glyphSize = Width;
			float[] cornerCounts = new float[4];

			for (int count = 0; count < 4; count++)
			{
				cornerCounts[count] = 0.0f;
				fourCorners[count] = new float2(0, 0);
			}
			for (int dy = 0; dy < glyphSize; dy++)
			{
				for (int dx = 0; dx < glyphSize; dx++)
				{
					int pix00 = GetPixel(dx, dy);
					float alphaX = dx / (glyphSize - 1.0f);
					float alphaY = dy / (glyphSize - 1.0f);
					float alphaNX = (1.0f - alphaX);
					float alphaNY = (1.0f - alphaY);

					if (pix00 > 128)
					{
						fourCorners[0] += new float2(dx, dy) * alphaNX * alphaNY;
						fourCorners[1] += new float2(dx, dy) * alphaX * alphaNY;
						fourCorners[2] += new float2(dx, dy) * alphaX * alphaY;
						fourCorners[3] += new float2(dx, dy) * alphaNX * alphaY;
						cornerCounts[0] += alphaNX * alphaNY;
						cornerCounts[1] += alphaX * alphaNY;
						cornerCounts[2] += alphaX * alphaY;
						cornerCounts[3] += alphaNX * alphaY;
					}
				}
			}
			if (cornerCounts[0] != 0.0f) fourCorners[0] /= cornerCounts[0];
			if (cornerCounts[1] != 0.0f) fourCorners[1] /= cornerCounts[1];
			if (cornerCounts[2] != 0.0f) fourCorners[2] /= cornerCounts[2];
			if (cornerCounts[3] != 0.0f) fourCorners[3] /= cornerCounts[3];
//			fourCorners[0] /= glyphSize;
//			fourCorners[1] /= glyphSize;
//			fourCorners[2] /= glyphSize;
//			fourCorners[3] /= glyphSize;
		}

		public float2 ComputeCOM()
		{
			float totalPixelCount = 0.0f;

			float2 COM = new float2(0, 0);
			for (int dy = 0; dy < Height; dy++)
			{
				for (int dx = 0; dx < Width; dx++)
				{
					int pix00 = GetPixel(dx, dy);

					if (pix00 >= 128)
					{
						COM += new float2(dx, dy);
						totalPixelCount += 1.0f;
					}
				}
			}
			COM /= totalPixelCount;
			return COM;
		}

		public float2 ComputeSpread(float2 COM)
		{
			int glyphSize = Width;
			float rightCount = 0.0f;
			float leftCount = 0.0f;
			float left = 0.0f;
			float right = 0.0f;

			for (int dy = 0; dy < glyphSize; dy++)
			{
				for (int dx = 0; dx < glyphSize; dx++)
				{
					int pix00 = GetPixel(dx, dy);

					if (pix00 >= 128)
					{
						if (dx > COM.x)
						{
							right += dx;
							rightCount += 1.0f;
						}
						else if (dx < COM.x)
						{
							left += dx;
							leftCount += 1.0f;
						}
						else // dx == COM.x
						{
							left += dx;
							right += dx;
							leftCount += 0.5f;
							rightCount += 0.5f;
						}
					}
				}
			}
			right /= rightCount;
			left /= leftCount;
			return new float2(left, right);
		}

		public float ComputePCA(float2 COM, int d0, int d1)
		{
			int glyphSize = Width;
			float totalPixelCount = 0.0f;

			float COV = 0.0f;
			for (int dy = 0; dy < glyphSize; dy++)
			{
				for (int dx = 0; dx < glyphSize; dx++)
				{
					int pix00 = GetPixel(dx, dy);
					float alphaX = (dx / (glyphSize - 1.0f)) * 2.0f - 1.0f;
					float alphaY = (dy / (glyphSize - 1.0f)) * 2.0f - 1.0f;

					if (pix00 > 128)
					{
						if ((d0 == 0) && (d1 == 0))
						{
							COV += (dx - COM.x) * (dx - COM.x);
						}
						else if ((d0 == 0) && (d1 == 1))
						{
							COV += (dx - COM.x) * (dy - COM.y);
						}
						else if ((d0 == 1) && (d1 == 0))
						{
							COV += (dy - COM.y) * (dx - COM.x);
						}
						else if ((d0 == 1) && (d1 == 1))
						{
							COV += (dy - COM.y) * (dy - COM.y);
						}
						totalPixelCount += 1.0f;
					}
				}
			}
			COV /= (totalPixelCount - 1);
			return COV;
		}

		private float COV(float x, float y, float cx, float cy)
		{
			return (x - cx) * (y - cy);
		}

		//public float[,] ComputePCA2(float2 COM)
		//{
		//    float[,] eigenBasis = new float[2, 2];

		//    int glyphSize = Width;
		//    float totalPixelCount = 0.0f;

		//    float3x3 cov = new float3x3();
		//    for (int dy = 0; dy < glyphSize; dy++)
		//    {
		//        for (int dx = 0; dx < glyphSize; dx++)
		//        {
		//            int pix00 = GetPixel(dx, dy);
		//            if (pix00 > 128)
		//            {
		//                cov[0, 0] += COV(dx, dx, COM.x, COM.x);
		//                cov[1, 1] += COV(dy, dy, COM.y, COM.y);
		//                cov[2, 2] += 0.0f;

		//                cov[0, 1] += COV(dx, dy, COM.x, COM.y);
		//                cov[1, 0] += COV(dx, dy, COM.x, COM.y);

		//                cov[0, 2] += COV(dx, 0, COM.x, 0);
		//                cov[2, 0] += COV(dx, 0, COM.x, 0);

		//                cov[1, 2] += COV(dy, 0, COM.y, 0);
		//                cov[2, 1] += COV(dy, 0, COM.y, 0);
		//                totalPixelCount += 1.0f;
		//            }
		//        }
		//    }
		//    float inv = 1.0f / totalPixelCount;
		//    cov.m00 *= inv; cov.m01 *= inv; cov.m02 *= inv;
		//    cov.m10 *= inv; cov.m11 *= inv; cov.m12 *= inv;
		//    cov.m20 *= inv; cov.m21 *= inv; cov.m22 *= inv;

		//    float3 evals = new float3();
		//    float3x3 evecs = new float3x3();
		//    Eigen.SymmetricSolver(cov, ref evals, ref evecs);

		//    // so now we have a new basis... we want the x and y vectors
		//    // make sure they are orthogonal, and normalized
		//    {
		//        float d2 = (evecs[0, 0] * evecs[0, 0]) + (evecs[0, 1] * evecs[0, 1]);
		//        if (d2 <= 0.0f) throw new Exception("0th eigen vectors are not positive vectors!");
		//        inv = 1.0f / (float)System.Math.Sqrt(d2);
		//        evecs[0, 0] *= inv;
		//        evecs[0, 1] *= inv;
		//    }
		//    {
		//        float d2 = (evecs[1, 0] * evecs[1, 0]) + (evecs[1, 1] * evecs[1, 1]);
		//        if (d2 <= 0.0f) throw new Exception("1st eigen vectors are not positive vectors!");
		//        inv = 1.0f / (float)System.Math.Sqrt(d2);
		//        evecs[1, 0] *= inv;
		//        evecs[1, 1] *= inv;
		//    }
		//    float dot = (evecs[0, 0] * evecs[1, 0]) + (evecs[0, 1] * evecs[1, 1]);
		//    if (dot < -1e-03f || dot > 1e-03f)
		//    {
		//        throw new Exception("eigen vectors are not orthogonal! " + dot.ToString());
		//    }

		//    // transpose
		//    eigenBasis[0, 0] = evecs[0, 0];
		//    eigenBasis[0, 1] = evecs[1, 0];
		//    eigenBasis[1, 0] = evecs[0, 1];
		//    eigenBasis[1, 1] = evecs[1, 1];

		//    return eigenBasis;
		//}

		public Box2 CopyRegionToGlyph(BitmapGray source, float2[] fourCorners, float multiSample)
		{
			Box2 bounds = new Box2();
			for (float y = 0; y < Height; y+=(1.0f/multiSample))	// do some super-sampling
			{
				for (float x = 0; x < Width; x += (1.0f / multiSample))
				{
					float alphaX = x / (Width - 1.0f);
					float alphaY = y / (Height - 1.0f);
					float alphaNX = (1.0f - alphaX);
					float alphaNY = (1.0f - alphaY);

					float xAlpha = x / (Width - 1.0f);
					float yAlpha = y / (Height - 1.0f);
					// clockwise from upper left [0-3]
					float2 UInterp = float2.Lerp(fourCorners[0], fourCorners[1], xAlpha);
					float2 LInterp = float2.Lerp(fourCorners[3], fourCorners[2], xAlpha);
					float2 interp = float2.Lerp(UInterp, LInterp, yAlpha);
					float2 photoUV = interp;

					int pix00 = source.GetPixelBilinearSafe(photoUV.x, photoUV.y);
					if (pix00 >= 128) bounds.MergePoint(new float2(x, y));
					//SetPixel(x, y, (byte)pix00);
					AddPixel((int)x, (int)y, (byte)(pix00 / (multiSample*multiSample)));	// do some super-sampling
				}
			}
			return bounds;
		}

		public BitmapGray DeItalicize(float skew)
		{
			BitmapGray result = new BitmapGray(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					byte pix00 = GetPixelClamped((int)(x + (skew*(y-Height/2))), y);
					result.SetPixel(x, y, pix00);
				}
			}
			return result;
		}

		public Box2 CopyToRegion(BitmapGray source, int destX, int destY)
		{
			Box2 bounds = new Box2();
			for (int y = 0; y < source.Height; y++)
			{
				for (int x = 0; x < source.Width; x++)
				{
					byte pix00 = source.GetPixel(x, y);
					if (pix00 >= 128) bounds.MergePoint(new float2(x + destX, y + destY));
					SetPixel(x + destX, y + destY, pix00);
				}
			}
			return bounds;
		}

		// When this computes the bounding box, it starts from the bottom so it can leave out the dot on 'i' and 'j'
		public Box2 CopyToRegionDottedTop(BitmapGray source, int destX, int destY)
		{
			Box2 bounds = new Box2();
			bool startedLine = false;
			for (int y = source.Height - 1; y >= 0; y--)
			{
				bool clearLine = true;
				for (int x = 0; x < source.Width; x++)
				{
					byte pix00 = source.GetPixel(x, y);
					if (pix00 >= 128)
					{
						bounds.MergePoint(new float2(x + destX, y + destY));
						clearLine = false;
						startedLine = true;
					}
					SetPixel(x + destX, y + destY, pix00);
				}
				if (clearLine && startedLine) break;
			}
			return bounds;
		}

		// When this computes the bounding box, it starts from the TOP so it only deals with the accent
		public Box2 CopyToRegionForAccents(BitmapGray source, int destX, int destY)
		{
			Box2 bounds = new Box2();
			bool startedLine = false;
			for (int y = 0; y < source.Height; y++)
			{
				bool clearLine = true;
				for (int x = 0; x < source.Width; x++)
				{
					byte pix00 = source.GetPixel(x, y);
					if (pix00 >= 128)
					{
						bounds.MergePoint(new float2(x + destX, y + destY));
						clearLine = false;
						startedLine = true;
					}
					SetPixel(x + destX, y + destY, pix00);
				}
				if (clearLine && startedLine) break;
			}
			return bounds;
		}

		public BitmapGray ConvertTo1Bit(byte threshold = 128)
		{
			BitmapGray result = new BitmapGray(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					byte pix00 = GetPixel(x, y);
					if (pix00 >= threshold) result.SetPixel(x, y, 255);
					else result.SetPixel(x, y, 0);
				}
			}
			return result;
		}

		// When this computes the bounding box, it starts from the bottom so it can leave out the dot on 'i' and 'j'
		public Box2 FindExtentsDottedTop()
		{
			Box2 extents = new Box2();
			bool startedLine = false;
			for (int y = Height - 1; y >= 0; y--)
			{
				bool clearLine = true;
				for (int x = 0; x < Width; x++)
				{
					if (GetPixel(x, y) >= 128)
					{
						extents.MergePoint(new float2(x, y));
						clearLine = false;
						startedLine = true;
					}
				}
				if (clearLine && startedLine) break;
			}
			return extents;
		}

		// When this computes the bounding box, it starts from the TOP so it only finds the accent
		public Box2 FindExtentsForAccents()
		{
			Box2 extents = new Box2();
			bool startedLine = false;
			for (int y = 0; y < Height; y++)
			{
				bool clearLine = true;
				for (int x = 0; x < Width; x++)
				{
					if (GetPixel(x, y) >= 128)
					{
						extents.MergePoint(new float2(x, y));
						clearLine = false;
						startedLine = true;
					}
				}
				if (clearLine && startedLine) break;
			}
			return extents;
		}

		public Box2 FindExtents()
		{
			Box2 extents = new Box2();
			for (int y = 0; y < Height; y++)
			{
				//for (int x = 0; x < Width; x++)
				int end = y * Width + Width;
				for (int x = y * Width; x < end; x++)
				{
					//if (GetPixel(x, y) >= 128)
					if (m_color[x] >= 128)
						extents.MergePoint(new float2(x % Width, y));
				}
			}
			return extents;
		}

		public void ClearDown(int startY)
		{
			for (int y = startY; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					SetPixel(x, y, 0);
				}
			}
		}

		public void ClearUp(int startY)
		{
			for (int y = startY; y >= 0; y--)
			{
				for (int x = 0; x < Width; x++)
				{
					SetPixel(x, y, 0);
				}
			}
		}

		public int CountPixelsOn()
		{
			int total = 0;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					byte pix00 = GetPixel(x, y);
					if (pix00 >= 128) total++;
				}
			}
			return total;
		}

		public void SaveBitmapAsH(string fname)
		{
			TextWriter tw = new StreamWriter(fname);

			tw.WriteLine("#pragma once");
			tw.WriteLine("//This file was auto-generated by the C# project called FontGenNormalized.");
			//tw.WriteLine("#include \"DataTypes.h\"");
			tw.WriteLine("static uint8 awesomeLetterGrid[] = {");

			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					byte pix = GetPixel(x, y);
					tw.Write(pix + ",");
				}
				tw.WriteLine();
			}
			tw.WriteLine("};");
			tw.Close();
		}

		public BitmapGray Levels()
		{
			BitmapGray result = new BitmapGray(Width, Height);
			int max = 0, min = 255;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					if (GetPixel(x, y) > max) max = GetPixel(x, y);
					if (GetPixel(x, y) < min) min = GetPixel(x, y);
				}
			}
			int delta = (int)max - (int)min;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					int pix = ((GetPixel(x, y) - min) * 255) / (max - min);
					result.SetPixel(x, y, (byte)pix);
				}
			}
			return result;
		}

		public int DotProductSpecial(BitmapGray pca)
		{
			int total = 0;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					int pcaPix = ((int)pca.GetPixel(x, y)) - 128;
					//int pcaPix = ((int)pca.GetPixel(x, y)) >= 128 ? 1 : 0;
					total += (int)GetPixel(x, y) * pcaPix;
				}
			}
			// scale it down so it fits happily in 8 bits.
			total = (total >> 8) / 16;
			if (total > 127) total = 127;
			if (total < -127) total = -127;
			return total;
		}

		public int[] ComputePCACoefficients(BitmapGray[] allPCA)
		{
			int[] result = new int[allPCA.Length];
			for (int count = 0; count < allPCA.Length; count++)
			{
				BitmapGray bg = allPCA[count];
				result[count] = DotProductSpecial(bg);
			}

			return result;
		}

		public static BitmapGray ReconstructFromPCA(BitmapGray[] allPCA, int[] coefficients, int numCoefficients, float NormFactor, int finalWidth, int finalHeight)
		{
			if ((numCoefficients > allPCA.Length) || (numCoefficients <= 0)) numCoefficients = allPCA.Length;
			BitmapGray result = new BitmapGray(allPCA[0].Width, allPCA[0].Height);
			for (int y = 0; y < result.Height; y++)
			{
				for (int x = 0; x < result.Width; x++)
				{
					float pix = 0;
					for (int count = 0; count < numCoefficients; count++)
					{
						BitmapGray bg = allPCA[count];
						float pcaPix = (((float)bg.GetPixel(x, y)) - 128) / 128;
						if (count == 0) pcaPix *= 8;	// is this right???
						if (count == 1) pcaPix *= 2;
						if (count == 2) pcaPix *= 2;
						pix += pcaPix * (coefficients[count]);
					}
					//pix /= (finalWidth * finalHeight);
					pix /= 2;	// SIZE DEPENDENT? WHAT IS THIS???? maybe because original bitmap is all positive instead of -1 to 1??
					pix *= NormFactor;
					pix *= 32;
					pix = System.Math.Max(0, pix);
					pix = System.Math.Min(255, pix);
					result.SetPixel(x, y, (byte)(pix));
				}
			}

			return result;
		}

		public int DotProductSpecialUnnormalized(BitmapGray pca)
		{
			int total = 0;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					int pcaPix = ((int)pca.GetPixel(x, y)) - 128;
					//int pcaPix = ((int)pca.GetPixel(x, y)) >= 128 ? 1 : 0;
					total += (int)GetPixel(x, y) * pcaPix;
				}
			}
			return total;
		}

		public int[] ComputePCACoefficientsUnnormalized(List<BitmapGray> allPCA)
		{
			int[] result = new int[allPCA.Count];
			for (int count = 0; count < allPCA.Count; count++)
			{
				BitmapGray bg = allPCA[count];
				result[count] = DotProductSpecial(bg);
			}

			return result;
		}

		public static BitmapGray ReconstructFromPCAUnnormalized(List<BitmapGray> allPCABasisVectors, int[] coefficients, int numCoefficients, float NormFactor = 1.0f)
		{
			if ((numCoefficients > allPCABasisVectors.Count) || (numCoefficients <= 0)) numCoefficients = allPCABasisVectors.Count;
			BitmapGray result = new BitmapGray(allPCABasisVectors[0].Width, allPCABasisVectors[0].Height);
			for (int y = 0; y < result.Height; y++)
			{
				for (int x = 0; x < result.Width; x++)
				{
					float pix = 0;
					for (int count = 0; count < numCoefficients; count++)
					{
						BitmapGray bg = allPCABasisVectors[count];
						float pcaPix = (((float)bg.GetPixel(x, y)) - 128) / 128;
						pix += pcaPix * (coefficients[count]);
					}
					//pix /= (finalWidth * finalHeight);
					//pix /= 2;	// SIZE DEPENDENT? WHAT IS THIS???? maybe because original bitmap is all positive instead of -1 to 1??
					pix *= NormFactor;
					//pix *= 32;
					pix = System.Math.Max(0, pix);
					pix = System.Math.Min(255, pix);
					result.SetPixel(x, y, (byte)(pix));
				}
			}

			return result;
		}

		public BitmapGray Crop(int sourceX, int sourceY, int cropWidth, int cropHeight)
		{
			BitmapGray result = new BitmapGray(cropWidth, cropHeight);
			for (int y = 0; y < cropHeight; y++)
			{
				for (int x = 0; x < cropWidth; x++)
				{
					result.SetPixel(x, y, GetPixel(x + sourceX, y + sourceY));
				}
			}
			return result;
		}

		public int BitmapDistance(BitmapGray bg)
		{
			int dist = 0;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					if (GetPixel(x, y) != bg.GetPixel(x, y)) dist++;
				}
			}
			return dist;
		}

		public BitmapGray DownSample2x2()
		{
			BitmapGray result = new BitmapGray(Width / 2, Height / 2);
			for (int y = 0; y < result.Height; y++)
			{
				for (int x = 0; x < result.Width; x++)
				{
					int pix00 = GetPixel(x * 2, y * 2);
					int pix10 = GetPixel(x * 2 + 1, y * 2);
					int pix01 = GetPixel(x * 2, y * 2 + 1);
					int pix11 = GetPixel(x * 2 + 1, y * 2 + 1);
					result.SetPixel(x, y, (byte)((pix00 + pix01 + pix10 + pix11) / 4));
				}
			}
			return result;
		}

		public void Amplify(float amount)
		{
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					int pix = GetPixel(x, y);
					pix -= 128;
					pix = (int)(pix * amount);
					pix += 128;
					if (pix > 255) pix = 255;
					if (pix < 0) pix = 0;
					SetPixel(x, y, (byte)pix);
				}
			}
		}

		public void CopyFromScaledRegion(BitmapGray source, Box2 region)
		{
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float alphaX = (float)x / (Width-0);
					float alphaY = (float)y / (Height-0);
					int pix = source.GetPixelBilinearSafe(region.Min.x + region.Size().x * alphaX, region.Min.y + region.Size().y * alphaY);
					SetPixel(x, y, (byte)pix);
				}
			}
		}

		public int Diff(BitmapGray source, int currentBest)
		{
			int total = 0;
			//for (int y = 0; y < Height; y++)
			//{
			//    for (int x = 0; x < Width; x++)
			//    {
			//        //total += Math.Abs(source.GetPixel(x, y) - GetPixel(x, y));
			//        int diff = (source.GetPixel(x, y) - GetPixel(x, y));
			//        total += diff * diff;
			//    }
			//    //if (total > currentBest) return total;	// early exit optimization
			//}
			//for (int count = 0; count < m_color.Length; count++)
			//{
			//    int diff = source.m_color[count] - m_color[count];
			//    total += diff * diff;
			//}

			unsafe
			{
				unchecked
				{
					int size = m_color.Length;
					fixed (byte* abase = &source.m_color[0], bbase = &m_color[0])
					{
						for (int k = 0; k < size; k++)
						{
							int diff = (int)abase[k] - (int)bbase[k];
							total += diff * diff;
						}
					}
				}
			}
			return total;

			//return m_color.Zip(source.m_color, (a, b) => (a - b) * (a - b)).Sum();
		}

		public List<BlobGray> FindIslandsDetailed(int threshold, int sign)
		{
			BitmapGray tempBmp = new BitmapGray(this);
			List<BlobGray> islands = new List<BlobGray>();
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					BlobGray blob = new BlobGray();
					blob.bmp = tempBmp.SegmentToBitmap(new int2(x, y), threshold, sign, out blob.bounds, out blob.pixelCount, out blob.pixels);
					if (blob.bounds.IsDefined())
					{
						islands.Add(blob);
	}
				}
			}
			return islands;
		}

		// Flood-fill and save out blob.
		public BitmapGray SegmentToBitmap(int2 p1, int threshold, int sign, out BoxI2 bounds, out int pixCount, out List<int2> allPixels)
		{
			List<int2> fillList = new List<int2>();
			allPixels = new List<int2>();

			pixCount = 0;
			bounds = new BoxI2();
			int2 current = p1;
			bool done = false;
			do
			{
				//float4 pix = GetPixel4((int)current.x, (int)current.y);
				int pix = GetPixelClamped(current.x, current.y);
				int x = current.x;
				bool good = pix < threshold;
				if (sign < 0) good = !good;
				if (good)
				{
					while (good)
					{
						bounds.MergePoint(new int2(x, current.y));
						fillList.Add(new int2(x + 1, current.y - 1));
						fillList.Add(new int2(x - 1, current.y - 1));
						fillList.Add(new int2(x, current.y - 1));
						fillList.Add(new int2(x + 1, current.y + 1));
						fillList.Add(new int2(x - 1, current.y + 1));
						fillList.Add(new int2(x, current.y + 1));
						//SetPixel((int)x, (int)current.y, new float4(pix.x, pix.y, pix.z, id));
						SetPixel(x, current.y, (byte)(threshold + sign));
						allPixels.Add(new int2(x, current.y));
						x += 1;
						//pix = GetPixel4((int)x, (int)current.y);
						pix = GetPixelClamped(x, current.y);
						good = pix < threshold;
						if (sign < 0) good = !good;
					}
					x = current.x - 1;
					//pix = GetPixel4((int)x, (int)current.y);
					pix = GetPixelClamped(x, current.y);
					good = pix < threshold;
					if (sign < 0) good = !good;
					while (good)
					{
						bounds.MergePoint(new int2(x, current.y));
						fillList.Add(new int2(x + 1, current.y - 1));
						fillList.Add(new int2(x - 1, current.y - 1));
						fillList.Add(new int2(x, current.y - 1));
						fillList.Add(new int2(x + 1, current.y + 1));
						fillList.Add(new int2(x - 1, current.y + 1));
						fillList.Add(new int2(x, current.y + 1));
						//SetPixel((int)x, (int)current.y, new float4(pix.x, pix.y, pix.z, id));
						SetPixel(x, current.y, (byte)(threshold + sign));
						allPixels.Add(new int2(x, current.y));
						x -= 1;
						//pix = GetPixel4((int)x, (int)current.y);
						pix = GetPixelClamped(x, current.y);
						good = pix < threshold;
						if (sign < 0) good = !good;
					}
				}
				if (fillList.Count > 0)
				{
					current = fillList[fillList.Count - 1];
					fillList.RemoveAt(fillList.Count - 1);
					while ((!IsInBounds((int)current.x, (int)current.y)) && (fillList.Count > 0))
					{
						current = fillList[fillList.Count - 1];
						fillList.RemoveAt(fillList.Count - 1);
					}
				}
				else done = true;
			} while (!done);

			if (!bounds.IsDefined()) return null;
			BitmapGray piece = new BitmapGray(bounds.Size().x + 3, bounds.Size().y + 3);
			foreach (var pos in allPixels)
			{
				byte pix = GetPixelClamped(pos.x, pos.y);
				//piece.SetPixel(pos.x - bounds.m_min.x + 1, pos.y - bounds.m_min.y + 1, pix);
				piece.SetPixel(pos.x - bounds.m_min.x + 1, pos.y - bounds.m_min.y + 1, (byte)255);
				//float4 pix = GetPixel4(pos.x, pos.y);
				//piece.SetPixel(pos.x - bounds.m_min.x + 1, pos.y - bounds.m_min.y + 1, new float4(pix.x, pix.y, pix.z, 255.0f));
			}
			pixCount = allPixels.Count;
			return piece;
		}

        public BitmapGray ScaleUpInteger(int scale)
        {
            BitmapGray target = new BitmapGray(Width * scale, Height * scale);
            for (int y = 0; y < target.Height; y++)
            {
                for (int x = 0; x < target.Width; x++)
                {
                    byte pix = GetPixel(x / scale, y / scale);
                    target.SetPixel(x, y, pix);
                }
            }
            return target;
        }


	}

	public class BlobGray
	{
		public BitmapGray bmp = new BitmapGray();
		public BoxI2 bounds = new BoxI2();
		public int pixelCount;
		public List<int2> pixels = new List<int2>();
	}

}

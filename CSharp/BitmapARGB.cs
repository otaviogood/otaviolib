﻿#region ApacheLicense2.0
// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion
#define BITMAPARGB

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Otavio.Math;

namespace Otavio.Image
{
	public class BitmapARGB
	{
		private int width;
		public int Width
		{
			get { return width; }
			set { width = value; }
		}

		private int height;
		public int Height
		{
			get { return height; }
			set { height = value; }
		}

		public int[] data;

		public void ResetSize(int w, int h)
		{
			Height = h;
			Width = w;
			data = new int[Width * Height];
		}

		public BitmapARGB()
		{
			ResetSize(8, 8);
		}

		public BitmapARGB(int w, int h)
		{
			ResetSize(w, h);
		}

		public BitmapARGB(string fileName)
		{
			FileLoad(fileName);
		}

		public BitmapARGB(BitmapARGB orig)
		{
			width = orig.Width;
			height = orig.Height;
			data = new int[orig.Width * orig.Height];
			data = (int[])orig.data.Clone();
		}

		public void Clear(UInt32 color)
		{
			for (int count = 0; count < data.Length; count++) data[count] = (int)color;
		}

		public bool IsInBounds(int x, int y)
		{
			if (x < 0) return false;
			if (y < 0) return false;
			if (x >= Width) return false;
			if (y >= Height) return false;
			return true;
		}

		public void SetPixel(int x, int y, int r, int g, int b)
		{
			data[x + y * width] = (int)(((uint)0xff << 24) | ((uint)r << 16) | ((uint)g << 8) | (uint)b);
		}

		public void SetPixel(int x, int y, int a, int r, int g, int b)
		{
			data[x + y * width] = (int)(((uint)a << 24) | ((uint)r << 16) | ((uint)g << 8) | (uint)b);
		}

		public uint GetPixel(int x, int y)
		{
			return (uint)data[x + y * width];
		}

		public uint GetPixelWrap(int x, int y)
		{
			x = (x + width) % width;
			y = (y + height) % height;
			return (uint)data[x + y * width];
		}

		public void SetPixel(int x, int y, uint pix)
		{
			data[x + y * width] = (int)pix;
		}

		public void GetPixel(int x, int y, out int r, out int g, out int b)
		{
			uint temp = (uint)data[x + y * width];
			r = (int)(temp >> 16) & 0xff;
			g = (int)(temp >> 8) & 0xff;
			b = (int)temp & 0xff;
		}

		public void GetPixel(int x, int y, out int a, out int r, out int g, out int b)
		{
			uint temp = (uint)data[x + y * width];
			a = (int)(temp >> 24) & 0xff;
			r = (int)(temp >> 16) & 0xff;
			g = (int)(temp >> 8) & 0xff;
			b = (int)temp & 0xff;
		}

		public uint GetPixelClamped(int x, int y)
		{
			if (x < 0) x = 0;
			if (y < 0) y = 0;
			if (x >= width) x = width - 1;
			if (y >= height) y = height - 1;
			return (uint)data[x + y * width];
		}

		public void GetPixelClamped(int x, int y, out int r, out int g, out int b)
		{
			if (x < 0) x = 0;
			if (y < 0) y = 0;
			if (x >= width) x = width - 1;
			if (y >= height) y = height - 1;
			uint temp = (uint)data[x + y * width];
			r = (int)(temp >> 16) & 0xff;
			g = (int)(temp >> 8) & 0xff;
			b = (int)temp & 0xff;
		}

		public void GetPixelClamped(int x, int y, out int a, out int r, out int g, out int b)
		{
			if (x < 0) x = 0;
			if (y < 0) y = 0;
			if (x >= width) x = width - 1;
			if (y >= height) y = height - 1;
			uint temp = (uint)data[x + y * width];
			a = (int)(temp >> 24) & 0xff;
			r = (int)(temp >> 16) & 0xff;
			g = (int)(temp >> 8) & 0xff;
			b = (int)temp & 0xff;
		}

		public void SetPixelSafe(int x, int y, int r, int g, int b)
		{
			if (x < 0) return;
			if (y < 0) return;
			if (x >= width) return;
			if (y >= height) return;
			r = System.Math.Min(255, r);
			g = System.Math.Min(255, g);
			b = System.Math.Min(255, b);
			r = System.Math.Max(0, r);
			g = System.Math.Max(0, g);
			b = System.Math.Max(0, b);
			data[x + y * width] = (int)(((uint)0xff << 24) | ((uint)r << 16) | ((uint)g << 8) | (uint)b);
		}

		public void SetPixelSafe(int x, int y, int a, int r, int g, int b)
		{
			if (x < 0) return;
			if (y < 0) return;
			if (x >= width) return;
			if (y >= height) return;
			a = System.Math.Min(255, a);
			r = System.Math.Min(255, r);
			g = System.Math.Min(255, g);
			b = System.Math.Min(255, b);
			a = System.Math.Max(0, a);
			r = System.Math.Max(0, r);
			g = System.Math.Max(0, g);
			b = System.Math.Max(0, b);
			data[x + y * width] = (int)(((uint)a << 24) | ((uint)r << 16) | ((uint)g << 8) | (uint)b);
		}

		public void SetPixelSafe(int x, int y, uint col)
		{
			if (x < 0) return;
			if (y < 0) return;
			if (x >= width) return;
			if (y >= height) return;
			SetPixel(x, y, col);
		}

		// For floating point compatibility
		public float3 GetPixel3(int x, int y)
		{
			int r, g, b;
			GetPixelClamped(x, y, out r, out g, out b);
			return new float3(r, g, b);
		}

		// FLIPS TO RGBA FROM ARGB
		public float4 GetPixel4(int x, int y)
		{
			int a, r, g, b;
			GetPixelClamped(x, y, out a, out r, out g, out b);
			return new float4(r, g, b, a);
		}

		// FLIPS FROM RGBA TO ARGB
		public void SetPixel4(int x, int y, float4 pix)
		{
			SetPixelSafe(x, y, (int)pix.w, (int)pix.x, (int)pix.y, (int)pix.z);
		}

		public float4 GetPixelBilinear4(float2 pos)
		{
			if (pos.x >= Width) pos.x = Width - 1;
			if (pos.x < 0) pos.x = 0;
			if (pos.y >= Height) pos.y = Height - 1;
			if (pos.y < 0) pos.y = 0;
			float2 floor = new float2((float)System.Math.Floor(pos.x), (float)System.Math.Floor(pos.y));
			float2 remainder = pos - floor;
			float4 pix00 = GetPixel4((int)floor.x, (int)floor.y);
			float4 pix10 = GetPixel4((int)floor.x + 1, (int)floor.y);
			float4 pix01 = GetPixel4((int)floor.x, (int)floor.y + 1);
			float4 pix11 = GetPixel4((int)floor.x + 1, (int)floor.y + 1);

			float4 pixX0 = pix10 * remainder.x + pix00 * (1.0f - remainder.x);
			float4 pixX1 = pix11 * remainder.x + pix01 * (1.0f - remainder.x);
			float4 pixY = pixX1 * remainder.y + pixX0 * (1.0f - remainder.y);
			return pixY;
		}

		//static uint Lerp4(uint c1, uint c2, float alpha)
		//{
		//    int a = (int)(alpha * 256);
		//    ColorARGB result = 0x00000000;
		//    result.c.a = (c2.c.a * a + c1.c.a * (255 - a)) >> 8;
		//    result.c.r = (c2.c.r * a + c1.c.r * (255 - a)) >> 8;
		//    result.c.g = (c2.c.g * a + c1.c.g * (255 - a)) >> 8;
		//    result.c.b = (c2.c.b * a + c1.c.b * (255 - a)) >> 8;
		//    return result;
		//}

		//public float4 GetPixelBilinear4Fast(float2 pos)
		//{
		//    //if (pos.x >= Width) pos.x = Width - 1;
		//    //if (pos.x < 0) pos.x = 0;
		//    //if (pos.y >= Height) pos.y = Height - 1;
		//    //if (pos.y < 0) pos.y = 0;
		//    int2 floor = new int2((int)Math.Floor(pos.x), (int)Math.Floor(pos.y));
		//    float2 remainder = pos - new float2(floor);
		//    uint pix00 = GetPixel((int)floor.x, (int)floor.y);
		//    uint pix10 = GetPixel((int)floor.x + 1, (int)floor.y);
		//    uint pix01 = GetPixel((int)floor.x, (int)floor.y + 1);
		//    uint pix11 = GetPixel((int)floor.x + 1, (int)floor.y + 1);

		//    float4 pixX0 = pix10 * remainder.x + pix00 * (1.0f - remainder.x);
		//    float4 pixX1 = pix11 * remainder.x + pix01 * (1.0f - remainder.x);
		//    float4 pixY = pixX1 * remainder.y + pixX0 * (1.0f - remainder.y);
		//    return pixY;
		//}

		public System.Drawing.Bitmap Draw(bool UseAlpha = true, bool colortastic = false)
		{
			if (object.ReferenceEquals(data, null)) return null;

			System.Drawing.Bitmap finalColorBitmap = null;
			if (finalColorBitmap == null) finalColorBitmap = new System.Drawing.Bitmap(Width, Height);
			System.Drawing.Imaging.PixelFormat pf = System.Drawing.Imaging.PixelFormat.Format24bppRgb;
			if (UseAlpha) pf = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
			// GDI+ still lies to us - the return format is BGR, NOT RGB.
			System.Drawing.Imaging.BitmapData bmData = finalColorBitmap.LockBits(
				new System.Drawing.Rectangle(0, 0, Width, Height),
				System.Drawing.Imaging.ImageLockMode.ReadWrite, pf);
			int stride = bmData.Stride;
			System.IntPtr Scan0 = bmData.Scan0;
			unsafe
			{
				byte* p = (byte*)(void*)Scan0;
				int nOffset = stride - Width * 3;
				if (UseAlpha)
				{
					nOffset = stride - Width * 4;
				}
				if (UseAlpha && (!colortastic))
				{
					Marshal.Copy(data, 0, Scan0, Width * Height);
				}
				else
				{
					for (int y = 0; y < Height; ++y)
					{
						for (int x = 0; x < Width; ++x)
						{
							int r, g, b, a;
							GetPixel(x, y, out a, out r, out g, out b);
							//float4 lookup = GetPixel4(x, y);
							//lookup = lookup.Clamp(0.0f, 255.0f);
							p[2] = (byte)(r);
							p[1] = (byte)(g);
							p[0] = (byte)(b);
							if (colortastic)
							{
								p[0] = (byte)(System.Math.Min(b * 2, 255));
								p[1] = (byte)(System.Math.Min(b * 2, 255));
								p[2] = (byte)(System.Math.Min(b * 2, 255));
							}
						//	float4 th = Shred.ThresholdSignature(GetPixel4(x, y));
						//	p[0] = (byte)th.x;
						//	p[1] = (byte)th.y;
						//	p[2] = (byte)th.z;
							p += 3;
							if (UseAlpha)
							{
								p[0] = (byte)a;
								p++;
							}

						}
						p += nOffset;
					}
				}
			}
			finalColorBitmap.UnlockBits(bmData);

			return finalColorBitmap;
		}

		public void FileLoad(string filename)
		{
			System.Drawing.Image image = null;
			try
			{
				image = System.Drawing.Image.FromFile(filename);
			}
			catch
			{
				throw new Exception("Error loading file");
				//Console.WriteLine("Error loading");
				//return;
			}
			System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(image);
			ResetSize(image.Width, image.Height);
			//for (int y = 0; y < target.Height; y++)
			//    for (int x = 0; x < target.Width; x++)
			//    {
			//        target.SetPixel(x, y, new float3(
			//            bmp.GetPixel(x, y).R / 255.0f, bmp.GetPixel(x, y).G / 255.0f, bmp.GetPixel(x, y).B / 255.0f));
			//    }

			// GDI+ still lies to us - the return format is BGR, NOT RGB.
			System.Drawing.Imaging.BitmapData bmData = bmp.LockBits(
				new System.Drawing.Rectangle(0, 0, Width, Height),
				System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);//.Format24bppRgb);
			int stride = bmData.Stride;
			System.IntPtr Scan0 = bmData.Scan0;
				Marshal.Copy(Scan0, data, 0, Width * Height);
			//unsafe
				//{
			//    byte* p = (byte*)(void*)Scan0;
			//    int nOffset = stride - Width * 4;
			//    //for (int y = 0; y < Height; ++y)
			//    //{
			//    //    for (int x = 0; x < Width; ++x)
			//    //    {
			//    //        //float4 pix = new float4();
			//    //        //pix.x = p[2];
			//    //        //pix.y = p[1];
			//    //        //pix.z = p[0];
			//    //        //pix.w = p[3];
			//    //        //SetPixel(x, y, pix);
			//    //        SetPixel(x, y, p[3], p[2], p[1], p[0]);
			//    //        p += 4;
			//    //    }
			//    //    p += nOffset;
			//    //}
				//}
			bmp.UnlockBits(bmData);
			bmp.Dispose();
			image.Dispose();
		}

		public void FileSave(string filename)
		{
			System.Drawing.Bitmap bmp = Draw(true);
			try
			{
				if (filename.ToLower().EndsWith("tif")) bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Tiff);	// tiff saves alpha channel.
				else if (filename.ToLower().EndsWith("bmp")) bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Bmp);
				else if (filename.ToLower().EndsWith("png")) bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
				else throw new Exception("Invalid filetype for filesave: " + filename);
			}
			catch
			{
				Console.WriteLine("Error saving.");
			}
		}

#if BITMAPFLOAT1
		public BitmapFloat1 GetChannelAsFloat(int channelIndex)
		{
			BitmapFloat1 result = new BitmapFloat1(Width, Height);
			for (int y = 0; y < Height; ++y)
			{
				for (int x = 0; x < Width; ++x)
				{
					float4 pix = GetPixel4(x, y);
					if (channelIndex == 0) result.SetPixel(x, y, pix.w);	// a
					if (channelIndex == 1) result.SetPixel(x, y, pix.x);	// r
					if (channelIndex == 2) result.SetPixel(x, y, pix.y);	// g
					if (channelIndex == 3) result.SetPixel(x, y, pix.z);	// b
				}
			}
			return result;
		}
#endif

		public List<Blob> FindIslandsDetailed()
		{
			List<Blob> islands = new List<Blob>();
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					Blob blob = new Blob();
					blob.bmp = SegmentToBitmap(new int2(x, y), 255, out blob.bounds, out blob.pixelCount);
					if (blob.bounds.IsDefined())
					{
						islands.Add(blob);
					}
				}
			}
			return islands;
		}

		// Flood-fill and save out blob.
		public BitmapARGB SegmentToBitmap(int2 p1, int id, out BoxI2 bounds, out int pixCount)
		{
			List<int2> fillList = new List<int2>();
			List<int2> allPixels = new List<int2>();

			pixCount = 0;
			bounds = new BoxI2();
			int2 current = p1;
			bool done = false;
			do
			{
				int a, r, g, b;
				//float4 pix = GetPixel4((int)current.x, (int)current.y);
				GetPixelClamped((int)current.x, (int)current.y, out a, out r, out g, out b);
				int x = current.x;
				while (a != id)
				{
					bounds.MergePoint(new int2(x, current.y));
					fillList.Add(new int2(x + 1, current.y - 1));
					fillList.Add(new int2(x - 1, current.y - 1));
					fillList.Add(new int2(x, current.y - 1));
					fillList.Add(new int2(x + 1, current.y + 1));
					fillList.Add(new int2(x - 1, current.y + 1));
					fillList.Add(new int2(x, current.y + 1));
					//SetPixel((int)x, (int)current.y, new float4(pix.x, pix.y, pix.z, id));
					SetPixel(x, current.y, id, r, g, b);
					allPixels.Add(new int2(x, current.y));
					x += 1;
					//pix = GetPixel4((int)x, (int)current.y);
					GetPixelClamped(x, current.y, out a, out r, out g, out b);
				}
				x = current.x - 1;
				//pix = GetPixel4((int)x, (int)current.y);
				GetPixelClamped(x, current.y, out a, out r, out g, out b);
				while (a != id)
				{
					bounds.MergePoint(new int2(x, current.y));
					fillList.Add(new int2(x + 1, current.y - 1));
					fillList.Add(new int2(x - 1, current.y - 1));
					fillList.Add(new int2(x, current.y - 1));
					fillList.Add(new int2(x + 1, current.y + 1));
					fillList.Add(new int2(x - 1, current.y + 1));
					fillList.Add(new int2(x, current.y + 1));
					//SetPixel((int)x, (int)current.y, new float4(pix.x, pix.y, pix.z, id));
					SetPixel(x, current.y, id, r, g, b);
					allPixels.Add(new int2(x, current.y));
					x -= 1;
					//pix = GetPixel4((int)x, (int)current.y);
					GetPixelClamped(x, current.y, out a, out r, out g, out b);
				}
				if (fillList.Count > 0)
				{
					current = fillList[fillList.Count - 1];
					fillList.RemoveAt(fillList.Count - 1);
					while ((!IsInBounds((int)current.x, (int)current.y)) && (fillList.Count > 0))
					{
						current = fillList[fillList.Count - 1];
						fillList.RemoveAt(fillList.Count - 1);
					}
				}
				else done = true;
			} while (!done);

			if (!bounds.IsDefined()) return null;
			BitmapARGB piece = new BitmapARGB(bounds.Size().x + 3, bounds.Size().y + 3);
			foreach (var pos in allPixels)
			{
				int a, r, g, b;
				GetPixel(pos.x, pos.y, out a, out r, out g, out b);
				piece.SetPixel(pos.x - bounds.m_min.x + 1, pos.y - bounds.m_min.y + 1, 255, r, g, b);
				//float4 pix = GetPixel4(pos.x, pos.y);
				//piece.SetPixel(pos.x - bounds.m_min.x + 1, pos.y - bounds.m_min.y + 1, new float4(pix.x, pix.y, pix.z, 255.0f));
			}
			pixCount = allPixels.Count;
			return piece;
		}

		public BitmapARGB ThresholdRedIntoAlpha(int threshold)
		{
			BitmapARGB target = new BitmapARGB(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					int a, r, g, b;
					GetPixel(x, y, out a, out r, out g, out b);
					target.SetPixel(x, y, ((r<threshold)?(int)0:(int)255), r, g, b);
				}
			}
			return target;
		}

		bool IsGreen(float3 pix)
		{
			if ((pix.x < 16) && (pix.z < 16) && (pix.y > 64)) return true;
			return false;
		}

		public void ErasePink(ref BitmapARGB target)
		{
			target.ResetSize(Width, Height);

			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					float3 pix1 = GetPixel3(x, y);
					//float3 pixUp = GetPixel3(x, y - 1);
					//float3 pixDown = GetPixel3(x, y + 1);
					//float3 pixRight = GetPixel3(x + 1, y);
					//float3 pixLeft = GetPixel3(x - 1, y);
					float4 pix = new float4(pix1.x, pix1.y, pix1.z, 255.0f);
					// Magic number for puzzle #2 green hack
					//if ((!IsGreen(pixUp)) && (!IsGreen(pixDown)) && (!IsGreen(pixRight)) && (!IsGreen(pixLeft)))
					//{
					if ((pix.x > pix.y * 1.6f) && (pix.x > pix.z) && (pix.x > 128)) pix.w = 0;
					//}
					target.SetPixel4(x, y, pix);
				}
			}
		}

		public BitmapARGB Flip180Lossless()
		{
			BitmapARGB target = new BitmapARGB(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					uint pix = GetPixel(x, y);
					target.SetPixel((Width - 1) - x, (Height - 1) - y, pix);
				}
			}
			return target;
		}

		public BitmapARGB FlipVertical()
		{
			BitmapARGB target = new BitmapARGB(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					uint pix = GetPixel(x, y);
					target.SetPixel(x, (Height - 1) - y, pix);
				}
			}
			return target;
		}

		public BitmapARGB ScaleUpInteger(int scale)
		{
			BitmapARGB target = new BitmapARGB(Width * scale, Height * scale);
			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					uint pix = GetPixel(x / scale, y / scale);
					target.SetPixel(x, y, pix);
				}
			}
			return target;
		}



		public List<BoxI2> FindIslands()
		{
			List<BoxI2> islands = new List<BoxI2>();
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					BoxI2 island = FloodFillIsland(new int2(x, y), 0);
					if (island.IsDefined())
					{
						islands.Add(island);
					}
				}
			}
			return islands;
		}

		// Flood-fill and save out blob.
		public BoxI2 FloodFillIsland(int2 p1, int id)
		{
			List<int2> fillList = new List<int2>();
			List<int2> allPixels = new List<int2>();

			BoxI2 bounds = new BoxI2();
			int2 current = p1;
			bool done = false;
			do
			{
				int a, r, g, b;
				//float4 pix = GetPixel4((int)current.x, (int)current.y);
				GetPixelClamped((int)current.x, (int)current.y, out a, out r, out g, out b);
				int x = current.x;
				while (a != id)
				{
					bounds.MergePoint(new int2(x, current.y));
					fillList.Add(new int2(x + 1, current.y - 1));
					fillList.Add(new int2(x - 1, current.y - 1));
					fillList.Add(new int2(x, current.y - 1));
					fillList.Add(new int2(x + 1, current.y + 1));
					fillList.Add(new int2(x - 1, current.y + 1));
					fillList.Add(new int2(x, current.y + 1));
					//SetPixel((int)x, (int)current.y, new float4(pix.x, pix.y, pix.z, id));
					SetPixel(x, current.y, id, r, g, b);
					allPixels.Add(new int2(x, current.y));
					x += 1;
					//pix = GetPixel4((int)x, (int)current.y);
					GetPixelClamped(x, current.y, out a, out r, out g, out b);
				}
				x = current.x - 1;
				//pix = GetPixel4((int)x, (int)current.y);
				GetPixelClamped(x, current.y, out a, out r, out g, out b);
				while (a != id)
				{
					bounds.MergePoint(new int2(x, current.y));
					fillList.Add(new int2(x + 1, current.y - 1));
					fillList.Add(new int2(x - 1, current.y - 1));
					fillList.Add(new int2(x, current.y - 1));
					fillList.Add(new int2(x + 1, current.y + 1));
					fillList.Add(new int2(x - 1, current.y + 1));
					fillList.Add(new int2(x, current.y + 1));
					//SetPixel((int)x, (int)current.y, new float4(pix.x, pix.y, pix.z, id));
					SetPixel(x, current.y, id, r, g, b);
					allPixels.Add(new int2(x, current.y));
					x -= 1;
					//pix = GetPixel4((int)x, (int)current.y);
					GetPixelClamped(x, current.y, out a, out r, out g, out b);
				}
				if (fillList.Count > 0)
				{
					current = fillList[fillList.Count - 1];
					fillList.RemoveAt(fillList.Count - 1);
					while ((!IsInBounds((int)current.x, (int)current.y)) && (fillList.Count > 0))
					{
						current = fillList[fillList.Count - 1];
						fillList.RemoveAt(fillList.Count - 1);
					}
				}
				else done = true;
			} while (!done);
			return bounds;
		}


		private bool IsRedShadow(float4 pix)
		{
			return pix.x > pix.y * 2.0f && pix.x > pix.z * 2.0f && pix.x > 40.0f;
		}

		public static float3 RGB2HSV(float3 pix)
		{
			float3 workingColor = new float3(pix.x, pix.y, pix.z);
			float h, s, v;
			float minColor, maxColor, delta;
			minColor = System.Math.Min(System.Math.Min(workingColor.x, workingColor.y), workingColor.z);
			maxColor = System.Math.Max(System.Math.Max(workingColor.x, workingColor.y), workingColor.z);
			v = maxColor;				// v
			delta = maxColor - minColor;
			if (delta != 0)		// This is different than the algorithm on the net. That one was buggy.
			{
				s = delta / maxColor;		// s

				if (workingColor.x == maxColor)
					h = (workingColor.y - workingColor.z) / delta;		// between yellow & magenta
				else if (workingColor.y == maxColor)
					h = 2 + (workingColor.z - workingColor.x) / delta;	// between cyan & yellow
				else
					h = 4 + (workingColor.x - workingColor.y) / delta;	// between magenta & cyan

				h *= 60;				// degrees
				if (h < 0)
					h += 360;
			}
			else
			{
				// r = g = b = 0		// s = 0, v is undefined
				s = 0;
				h = 0;
			}
			h /= 360.0f;
			return new float3(h, s, v);
		}

		private float3 HSV2RGB(float3 hsv)
		{
			float3 workingColor = new float3();
			float h = hsv.x * 360.0f;
			float s = hsv.y;
			float v = hsv.z;
			float i;
			float f, p, q, t;
			if (s == 0)
			{
				// achromatic (grey)
				workingColor.x = workingColor.y = workingColor.z = v;
			}
			else
			{
				h /= 60.0f;			// sector 0 to 5
				i = (float)System.Math.Floor(h);
				f = h - i;			// factorial part of h
				p = v * (1 - s);
				q = v * (1 - s * f);
				t = v * (1 - s * (1 - f));
				if (i == 0)
				{
					workingColor.x = v;
					workingColor.y = t;
					workingColor.z = p;
				}
				else if (i == 1)
				{
					workingColor.x = q;
					workingColor.y = v;
					workingColor.z = p;
				}
				else if (i == 2)
				{
					workingColor.x = p;
					workingColor.y = v;
					workingColor.z = t;
				}
				else if (i == 3)
				{
					workingColor.x = p;
					workingColor.y = q;
					workingColor.z = v;
				}
				else if (i == 4)
				{
					workingColor.x = t;
					workingColor.y = p;
					workingColor.z = v;
				}
				else
				{
					workingColor.x = v;
					workingColor.y = p;
					workingColor.z = q;
				}
			}
			return workingColor;
		}

		private bool IsPink(float4 pix, float avg)
		{
			/*
			float3 hsv = RGB2HSV(pix.xyz);
			hsv *= 255.0f;
			if ((hsv.x < 240.0f || hsv.x < 15.0f) && hsv.y > 32.0f)
			{
				return true;
			}
			return false;
			 * */
			//float navg = (avg - 160.0f) / (210.0f - 160.0f);
			//float red = 1.2f + 0.3f * (1.0f - navg);
			//return pix.x > pix.y * red && pix.x > pix.z * red && pix.x > 50.0f;
			return pix.x > pix.y * 1.35f && pix.x > pix.z * 1.3f && pix.x > 200.0f;
		}

		public BitmapARGB ScaleBilinear(float scale)
		{
			BitmapARGB target = new BitmapARGB((int)(Width * scale), (int)(Height * scale));
			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					// rotate around centroid.
					//float2 trans = new float2(x - Width / 2, y - Height / 2).Rotate(angle);
					float2 result = new float2();
					result.x = x / scale;
					result.y = y / scale;
					float4 pix = GetPixelBilinear4(result);
					target.SetPixel4(x, y, pix);
				}
			}
			return target;
		}

		// These arrays tell the program which of the 4 edges need to be interpolated to make
		// a line. They are pairs because they represent the line end points.
		int[,] lineIndex1 = new int[,] {{-1,-1}, {3,0}, {0,1}, {3,1}, {1,2}, {3,0}, {0,2}, {3,2},
										{2,3}, {2,0}, {0,1}, {2,1}, {1,3}, {1,0}, {0,3}, {-1,-1}};
		// It is possible (in 2 cases) that one block will make 2 lines. This array defines
		// the second line.
		int[,] lineIndex2 = new int[,] {{-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {1,2}, {-1,-1}, {-1,-1},
										{-1,-1}, {-1,-1}, {2,3}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}};

		// Pass a threshold value and this will turn a bitmap into an array of lines.

		private bool CheckAlpha(int x, int y)
		{
			float4 f00 = GetPixel4(x, y);
			float4 f10 = GetPixel4(x + 1, y);
			float4 f01 = GetPixel4(x, y + 1);
			float4 f11 = GetPixel4(x + 1, y + 1);
			if (f00.w == 0.0f || f10.w == 0.0f || f01.w == 0.0 || f11.w == 0.0f)
			{
				return true;
			}
			return false;
		}

		public List<float4> Vectorize(int threshold)
		{
			List<float4> v = new List<float4>();
			float2[] vertList = new float2[4];
			for (int y = 0; y < Height - 1; y++)
			{
				for (int x = 0; x < Width - 1; x++)
				{
					// this can be optimized by saying pix00 = pix10, pix01 = pix11 every time x is incremented.
					float4 f00 = GetPixel4(x, y);
					float4 f10 = GetPixel4(x + 1, y);
					float4 f01 = GetPixel4(x, y + 1);
					float4 f11 = GetPixel4(x + 1, y + 1);

					bool alphaLeft = CheckAlpha(x - 1, y);
					bool alphaRight = CheckAlpha(x + 1, y);
					if (alphaLeft || alphaRight || f00.w == 0.0f || f10.w == 0.0f || f01.w == 0.0 || f11.w == 0.0f)
					{
						continue;
					}

					float pix00 = f00.x;
					float pix10 = f10.x;
					float pix01 = f01.x;
					float pix11 = f11.x;

					int squareIndex = 0;
					if (pix00 < threshold) squareIndex |= 1;
					if (pix10 < threshold) squareIndex |= 2;
					if (pix11 < threshold) squareIndex |= 4;
					if (pix01 < threshold) squareIndex |= 8;
					if ((squareIndex == 0) || (squareIndex == 0xf)) continue;

					// middle of the pixel is in upper left. is this what we want?
					float2 pos00 = new float2((float)x, (float)y);
					float2 pos10 = new float2((float)(x + 1), (float)y);
					float2 pos01 = new float2((float)x, (float)(y + 1));
					float2 pos11 = new float2((float)(x + 1), (float)(y + 1));
					// interpolate the 4 edges. Not all 4 are always used, so with effort, this could be optimized.
					// also, the horizontal and vertical pos variables are very integery and this could be optimized.
					// these could also be shifted left like the other variables.
					vertList[0] = float2.Lerp(pos00, pos10, ((float)(threshold - pix00)) / ((float)(pix10 - pix00)));
					vertList[1] = float2.Lerp(pos10, pos11, ((float)(threshold - pix10)) / ((float)(pix11 - pix10)));
					vertList[2] = float2.Lerp(pos11, pos01, ((float)(threshold - pix11)) / ((float)(pix01 - pix11)));
					vertList[3] = float2.Lerp(pos01, pos00, ((float)(threshold - pix01)) / ((float)(pix00 - pix01)));

					int vertA = lineIndex1[squareIndex, 0];
					int vertB = lineIndex1[squareIndex, 1];
					int vert2A = lineIndex2[squareIndex, 0];
					int vert2B = lineIndex2[squareIndex, 1];

					// put final lines in the line list
					if (vertA != -1)
					{
						v.Add(new float4(vertList[vertA].x, vertList[vertA].y, vertList[vertB].x, vertList[vertB].y));
					}
					if (vert2A != -1)
					{
						v.Add(new float4(vertList[vert2A].x, vertList[vert2A].y, vertList[vert2B].x, vertList[vert2B].y));
					}
				}
			}
			return v;
		}

		public List<float4> Vectorize2x2(int x, int y, int threshold)
		{
			float2[] vertList = new float2[4];
			// this can be optimized by saying pix00 = pix10, pix01 = pix11 every time x is incremented.
			float4 f00 = GetPixel4(x, y);
			float4 f10 = GetPixel4(x + 1, y);
			float4 f01 = GetPixel4(x, y + 1);
			float4 f11 = GetPixel4(x + 1, y + 1);

			if (f00.w == 0.0f || f10.w == 0.0f || f01.w == 0.0 || f11.w == 0.0f)
			{
				return new List<float4>();
			}

			float pix00 = f00.x;
			float pix10 = f10.x;
			float pix01 = f01.x;
			float pix11 = f11.x;

			int squareIndex = 0;
			if (pix00 < threshold) squareIndex |= 1;
			if (pix10 < threshold) squareIndex |= 2;
			if (pix11 < threshold) squareIndex |= 4;
			if (pix01 < threshold) squareIndex |= 8;
			if ((squareIndex == 0) || (squareIndex == 0xf)) return new List<float4>();

			// middle of the pixel is in upper left. is this what we want?
			float2 pos00 = new float2((float)x, (float)y);
			float2 pos10 = new float2((float)(x + 1), (float)y);
			float2 pos01 = new float2((float)x, (float)(y + 1));
			float2 pos11 = new float2((float)(x + 1), (float)(y + 1));
			// interpolate the 4 edges. Not all 4 are always used, so with effort, this could be optimized.
			// also, the horizontal and vertical pos variables are very integery and this could be optimized.
			// these could also be shifted left like the other variables.
			vertList[0] = float2.Lerp(pos00, pos10, ((float)(threshold - pix00)) / ((float)(pix10 - pix00)));
			vertList[1] = float2.Lerp(pos10, pos11, ((float)(threshold - pix10)) / ((float)(pix11 - pix10)));
			vertList[2] = float2.Lerp(pos11, pos01, ((float)(threshold - pix11)) / ((float)(pix01 - pix11)));
			vertList[3] = float2.Lerp(pos01, pos00, ((float)(threshold - pix01)) / ((float)(pix00 - pix01)));

			int vertA = lineIndex1[squareIndex, 0];
			int vertB = lineIndex1[squareIndex, 1];
			int vert2A = lineIndex2[squareIndex, 0];
			int vert2B = lineIndex2[squareIndex, 1];

			// put final lines in the line list
			List<float4> v = new List<float4>();
			if (vertA != -1)
			{
				float4 vec = new float4(vertList[vertA].x, vertList[vertA].y, vertList[vertB].x, vertList[vertB].y);
				if (vec.xy.Distance(vec.zw) > 0.001f)
				{
					v.Add(vec);
				}
			}
			if (vert2A != -1)
			{
				float4 vec = new float4(vertList[vert2A].x, vertList[vert2A].y, vertList[vert2B].x, vertList[vert2B].y);
				if (vec.xy.Distance(vec.zw) > 0.001f)
				{
					v.Add(vec);
				}
			}
			return v;
		}

		public void MakeVectorSignature(out List<Wire> leftWires, out List<Wire> rightWires)
		{
			List<float4> vecSoup = new List<float4>();
			List<float4> leftEdge = new List<float4>();
			List<float4> rightEdge = new List<float4>();
			for (int y = 0; y < Height - 0; y++)
			{
				int x = 0;
				bool good = false;
				for (x = 0; x < Width; x++)
				{
					if (GetPixel4(x, y).w >= 255)
					{
						good = true;
						break;
					}
				}
				if (good)
				{
					for (int dx = 0; dx < 6; dx++)
					{
						List<float4> vecs = Vectorize2x2(x + dx + 2, y, 28);
						if (dx == 0 && vecs.Count > 0)
						{
							leftEdge.AddRange(vecs);
						}
						vecSoup.AddRange(vecs);
					}
				}
				int oldX = x;
				good = false;
				for (x = Width - 1; x > oldX - 6; x--)
				{
					if (GetPixel4(x, y).w >= 255)
					{
						good = true;
						break;
					}
				}
				if (GetPixel4(x, y).w >= 255)
				{
					for (int dx = 0; dx < 6; dx++)
					{
						List<float4> vecs = Vectorize2x2(x - dx - 2, y, 28);
						if (dx == 0 && vecs.Count > 0)
						{
							rightEdge.AddRange(vecs);
						}
						vecSoup.AddRange(vecs);
					}
				}
			}

			/*
			List<Wire> edgeWires = new List<Wire>();
			foreach (float4 vec in leftEdge)
			{
				Wire wire = new Wire();
				wire.vecs.Add(vec);
				edgeWires.Add(wire);
			}
			foreach (float4 vec in rightEdge)
			{
				Wire wire = new Wire();
				wire.vecs.Add(vec);
				edgeWires.Add(wire);
			}

			return edgeWires;
			 * */

			List<Wire> wires = new List<Wire>();
			foreach (var vec in vecSoup)
			{
				Wire wire = new Wire();
				wire.vecs.Add(vec);
				wire.head = vec.xy;
				wire.tail = vec.zw;
				wire.tailVec = vec;
				wire.headVec = vec;
				wire.orderedPoints.AddFirst(wire.head);
				wire.orderedPoints.AddLast(wire.tail);
				wire.length = vec.xy.Distance(vec.zw);
				wires.Add(wire);
			}

			bool added = true;
			while (added)
			{
				added = false;
				foreach (Wire wire in wires)
				{
					Wire closestWire = null;
					bool firstHead = false;
					bool secondHead = false;
					float precision = 2.0f;
					float closestDist = precision;
					foreach (Wire otherWire in wires)
					{
						if (wire != otherWire && !wire.removed && !otherWire.removed)
						{
							float headHead = wire.head.Distance(otherWire.head);
							float tailHead = wire.tail.Distance(otherWire.head);
							float headTail = wire.head.Distance(otherWire.tail);
							float tailTail = wire.tail.Distance(otherWire.tail);
							if (headHead < closestDist && headHead < tailHead && headHead < headTail && headHead < tailTail)
							{
								firstHead = true;
								secondHead = true;
								closestWire = otherWire;
								closestDist = headHead;
							}
							else if (tailHead < closestDist && tailHead < headTail && tailHead < tailTail)
							{
								firstHead = false;
								secondHead = true;
								closestWire = otherWire;
								closestDist = tailHead;
							}
							else if (headTail < closestDist && headTail < tailTail)
							{
								firstHead = true;
								secondHead = false;
								closestWire = otherWire;
								closestDist = headTail;
							}
							else if (tailTail < closestDist)
							{
								firstHead = false;
								secondHead = false;
								closestWire = otherWire;
								closestDist = tailTail;
							}
							if (closestDist == 0.0f)
							{
								break;
							}
						}
					}
					if (closestDist < precision)
					{
						if (firstHead && secondHead)
						{
							wire.head = closestWire.tail;
							wire.headVec = closestWire.tailVec;
							foreach (float2 point in closestWire.orderedPoints)
							{
								wire.orderedPoints.AddFirst(point);
							}
						}
						else if (!firstHead && secondHead)
						{
							wire.tail = closestWire.tail;
							wire.tailVec = closestWire.tailVec;
							foreach (float2 point in closestWire.orderedPoints)
							{
								wire.orderedPoints.AddLast(point);
							}
						}
						else if (firstHead && !secondHead)
						{
							wire.head = closestWire.head;
							wire.headVec = closestWire.headVec;
							wire.orderedPoints.AddFirst(closestWire.tail);
							wire.orderedPoints.AddFirst(closestWire.head);
							foreach (float2 point in closestWire.orderedPoints.Reverse())
							{
								wire.orderedPoints.AddFirst(point);
							}
						}
						else if (!firstHead && !firstHead)
						{
							wire.tail = closestWire.head;
							wire.tailVec = closestWire.headVec;
							wire.orderedPoints.AddLast(closestWire.tail);
							wire.orderedPoints.AddLast(closestWire.head);
							foreach (float2 point in closestWire.orderedPoints.Reverse())
							{
								wire.orderedPoints.AddLast(point);
							}
						}
						wire.length += closestWire.length;
						wire.vecs.AddRange(closestWire.vecs);
						closestWire.removed = true;
						added = true;
					}
				}
			}

			leftWires = new List<Wire>();
			rightWires = new List<Wire>();
			foreach (Wire wire in wires)
			{
				if (!wire.removed)
				{
					int numLeft = 0;
					foreach (float4 edge in leftEdge)
					{
						if (wire.headVec == edge)
						{
							numLeft++;
							wire.headImportant = true;
						}
						if (wire.tailVec == edge)
						{
							numLeft++;
							wire.tailImportant = true;
						}
					}
					if (numLeft == 1 || (numLeft == 2 && wire.length > 2))
					{
						leftWires.Add(wire);
					}
					int numRight = 0;
					foreach (float4 edge in rightEdge)
					{
						if (wire.headVec == edge)
						{
							numRight++;
							wire.headImportant = true;
						}
						if (wire.tailVec == edge)
						{
							numRight++;
							wire.tailImportant = true;
						}
					}
					if (numRight == 1 || (numRight == 2 && wire.length > 2))
					{
						rightWires.Add(wire);
					}
				}
			}
			float verticalNess = 0.85f;
			foreach (Wire wire in leftWires)
			{
				if (wire.headImportant)
				{
					List<float2> points = new List<float2>(wire.orderedPoints);
					float2 start = points[0];
					float2 end = points[1];
					for (int i = 1; i < points.Count; i++)
					{
						end = points[i];
						if (start.Distance(end) > (wire.tailImportant ? 1.0f : 2.0f))
						{
							break;
						}
					}
					float2 d = (end - start).Normalize();
					if (System.Math.Abs(d.y) < verticalNess && end.Distance(start) > 0.01f)
					{
						if (wire.headVec.xy == start)
						{
							wire.importantVectors.Add(new float4(start.x, start.y, end.x, end.y));
						}
						else
						{
							wire.importantVectors.Add(new float4(end.x, end.y, start.x, start.y));
						}
					}
				}
				if (wire.tailImportant)
				{
					List<float2> points = new List<float2>(wire.orderedPoints);
					float2 start = points[points.Count - 1];
					float2 end = points[points.Count - 2];
					for (int i = points.Count - 2; i >= 0; i--)
					{
						end = points[i];
						if (start.Distance(end) > (wire.headImportant ? 1.0f : 2.0f))
						{
							break;
						}
					}
					float2 d = (end - start).Normalize();
					if (System.Math.Abs(d.y) < verticalNess && end.Distance(start) > 0.01f)
					{
						if (wire.tailVec.xy == start)
						{
							wire.importantVectors.Add(new float4(start.x, start.y, end.x, end.y));
						}
						else
						{
							wire.importantVectors.Add(new float4(end.x, end.y, start.x, start.y));
						}
					}
				}
			}
			foreach (Wire wire in rightWires)
			{
				if (wire.headImportant)
				{
					List<float2> points = new List<float2>(wire.orderedPoints);
					float2 start = points[0];
					float2 end = points[1];
					for (int i = 1; i < points.Count; i++)
					{
						end = points[i];
						if (start.Distance(end) > (wire.tailImportant ? 1.0f : 2.0f))
						{
							break;
						}
					}
					float2 d = (end - start).Normalize();
					if (System.Math.Abs(d.y) < verticalNess && end.Distance(start) > 0.01f)
					{
						if (wire.headVec.xy == start)
						{
							wire.importantVectors.Add(new float4(start.x, start.y, end.x, end.y));
						}
						else
						{
							wire.importantVectors.Add(new float4(end.x, end.y, start.x, start.y));
						}
					}
				}
				if (wire.tailImportant)
				{
					List<float2> points = new List<float2>(wire.orderedPoints);
					float2 start = points[points.Count - 1];
					float2 end = points[points.Count - 2];
					for (int i = points.Count - 2; i >= 0; i--)
					{
						end = points[i];
						if (start.Distance(end) > (wire.headImportant ? 1.0f : 2.0f))
						{
							break;
						}
					}
					float2 d = (end - start).Normalize();
					if (System.Math.Abs(d.y) < verticalNess && end.Distance(start) > 0.01f)
					{
						if (wire.tailVec.xy == start)
						{
							wire.importantVectors.Add(new float4(start.x, start.y, end.x, end.y));
						}
						else
						{
							wire.importantVectors.Add(new float4(end.x, end.y, start.x, start.y));
						}
					}
				}
			}
		}
	}

	public class Wire
	{
		public List<float4> vecs = new List<float4>();
		public LinkedList<float2> orderedPoints = new LinkedList<float2>();
		public float2 head;
		public float4 headVec;
		public float2 tail;
		public float4 tailVec;
		public bool tailImportant;
		public bool headImportant;
		public List<float4> importantVectors = new List<float4>();
		public float length = 0.0f;
		public bool removed = false;
	}

	public class Blob
	{
		public BitmapARGB bmp = new BitmapARGB();
		public BoxI2 bounds = new BoxI2();
		public int pixelCount;
		public List<int2> pixels = new List<int2>();
	}
}
